# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    Authors: openerp
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from openerp.osv import osv, fields
from openerp.tools.translate import _


class account_account(osv.osv):
    _inherit = 'account.account'
    _columns = {
        'nature': fields.selection(
            [('D', 'Deudora'),
             ('A', 'Acreedora')
             ],
            string='Naturaleza',

        ),
        'sat_group_id': fields.many2one(
            'account.account.sat_group', string='Grupo SAT',
            ondelete='set null',

        ),
    }

    def _check_account(self, cr, uid, ids, context=None):

        for account in self.browse(cr, uid, ids, context=context):
            if not account.sat_group_id and \
                account.type == "view" and \
                    account.code != "0":
                return False
        return True
    
    def _check_account_nature(self, cr, uid, ids, context=None):

        for account in self.browse(cr, uid, ids, context=context):
            if account.sat_group_id and not account.nature:
               return False
            return True 

    _constraints = [
        (_check_account, 'No definido un Grupo SAT para la cuenta!\n'
            'Cuentas de tipo vista deben tener Grupo SAT!',
            ['sat_group_id']),
        (_check_account_nature, 'No definida la Naturaleza de la Cuenta!\n'
            'Las cuentas deben tener asignada su Naturaleza',
            ['sat_group_id']),
    ]


class account_account_template(osv.Model):
    _inherit = 'account.account.template'
    _columns = {
        'nature': fields.selection(
            [('D', 'Deudora'),
             ('A', 'Acreedora')
             ],
            string='Naturaleza',

        ),
        'sat_group_id': fields.many2one(
            'account.account.sat_group', string='Grupo SAT',
            ondelete='set null',

        ),
    }

    def _check_account(self, cr, uid, ids, context=None):

        for account in self.browse(cr, uid, ids, context=context):
            if not account.sat_group_id and \
                account.type == "view" and \
                    account.code != "0":
                return False
        return True

    _constraints = [
        (_check_account, 'No definido un Grupo SAT para la cuenta!\n'
            'Cuentas de tipo vista deben tener Grupo SAT!!',
            ['sat_group_id']),
    ]
