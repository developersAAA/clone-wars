# __openerp__.py
{
    'name': "iKom Color",
    'description': "Cambiar colores a paleta de iKom",
    'category': 'Hidden',
    'depends': ['web'],
    'author': 'René Puig',
    'website': 'www.ikom.mx',
    'qweb': ['views/ikom-color.xml'],
    'css': ['static/src/css/ikom-color.css',],
    'data': ['views/ikom-color.xml'],
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

