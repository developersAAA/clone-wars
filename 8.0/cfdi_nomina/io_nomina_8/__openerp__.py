# -*- coding: utf-8 -*-
{
    'name' : 'io_nomina',
    'version' : '1.0',
    'author' : 'iozoft.com @glopzvega',
    'category' : 'RH',
    'sequence': 0,
    'description' : """
        Timbrado de Nomina
    """,
    'website': 'http://www.iozoft.com',
    'depends' : [
                 "gl_hr_payroll",
                 "io_base_vat",
                 "l10n_mx_facturae_pac_sf",                 
                ],
    
    'data': [
        "security/ir.model.access.csv",
        # 'data.xml',
        'menu.xml',
#         'hr_view.xml',
        'res_partner_empleado_view.xml',
        'invoice_nominas_view.xml',
        'io_catalogos_view.xml',
        'views/report_nomina.xml',
        'nomina_report.xml'
        
#         'product_view.xml',
#         'menu.xml'
             #'res_partner_view.xml'        
    ],
    'qweb' : [
    ],
    'demo': [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
