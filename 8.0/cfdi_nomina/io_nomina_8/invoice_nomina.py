# -*- coding: utf-8 -*-
from openerp import api
from openerp.osv import osv, fields
from openerp.exceptions import except_orm
from lxml import etree
import tempfile
import os
import codecs
import base64
import xml.dom.minidom
from datetime import datetime, timedelta
from openerp.tools.translate import _
try:
    from SOAPpy import WSDL
except:
    print "Package SOAPpy missed"
    pass
import time
from openerp.addons.l10n_mx_invoice_amount_to_text import amount_to_text_es_MX
from openerp import tools

class invoice_nomina(osv.osv):
#     _name = "account.invoice.nomina"
    _inherit = "account.invoice"
#     _table = "account_invoice"
#     _description = "Timbrado Nominas"
        
    def _is_nomina(self, cr, uid, context=None):        
        if context is not None and context.has_key("is_nomina"):
            return True        
        return False
    
    def _get_nomina_journal(self, cr, uid, context=None):
        usuario = self.pool.get("res.users").browse(cr, 1, uid, context)
        journal_obj = self.pool.get("account.journal")
        if context is not None and context.has_key("is_nomina") and context["is_nomina"]:            
            journal_ids = journal_obj.search(cr, uid, [("company_id", "=", usuario.company_id.id), 
                                                       ("code", "=", "NOM")])
            if journal_ids:
                return journal_ids[0]            
        else:
            journal_ids = journal_obj.search(cr, uid, [("company_id", "=", usuario.company_id.id), 
                                                       ("type", "=", "sale"),
                                                       ("code", "=", "VEN"),])
            if journal_ids:
                return journal_ids[0]
            
        return False
        
    @api.multi
    def invoice_nomina_print(self):
        """ Print the invoice with the new Template"""
        
        assert len(self) == 1, 'This option should only be used for a single id at a time.'
        self.sent = True
        return self.env['report'].get_action(self, 'io_nomina_8.report_nomina')
    
    @api.model
    def fields_view_get(self, view_id=None, view_type=False, toolbar=False, submenu=False):
        context = self._context

        def get_view_id(xid, name):
            try:
                return self.env['ir.model.data'].xmlid_to_res_id('account.' + xid, raise_if_not_found=True)
            except ValueError:
                try:
                    return self.env['ir.ui.view'].search([('name', '=', name)], limit=1).id
                except Exception:
                    return False    # view not found

        if context.get('active_model') == 'res.partner' and context.get('active_ids'):
            partner = self.env['res.partner'].browse(context['active_ids'])[0]
            if not view_type:
                view_id = get_view_id('invoice_tree', 'account.invoice.tree') 
                view_type = 'tree'
            elif view_type == 'form':
                if partner.supplier and not partner.customer:
                    view_id = get_view_id('invoice_supplier_form', 'account.invoice.supplier.form')
                elif partner.customer and not partner.supplier:
                    view_id = get_view_id('invoice_form', 'account.invoice.form')
        
        if context.get("is_nomina"):
            if view_type == "search":
                view_id = get_view_id('io_view_account_invoice_filter', 'io.account.invoice.select')
            elif view_type == "tree":
                view_id = get_view_id('io_nomina_view_tree', 'io.nomina.view.tree')
                
        res = super(invoice_nomina, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar, submenu=submenu)

        # adapt selection of field journal_id
        for field in res['fields']:
            if field == 'journal_id' and type:
                journal_select = self.env['account.journal']._name_search('', [('type', '=', type)], name_get_uid=1)
                res['fields'][field]['selection'] = journal_select

        doc = etree.XML(res['arch'])

        if context.get('type'):
            for node in doc.xpath("//field[@name='partner_bank_id']"):
                if context['type'] == 'in_refund':
                    node.set('domain', "[('partner_id.ref_companies', 'in', [company_id])]")
                elif context['type'] == 'out_refund':
                    node.set('domain', "[('partner_id', '=', partner_id)]")

        if view_type == 'search':
            if context.get('type') in ('out_invoice', 'out_refund'):
                for node in doc.xpath("//group[@name='extended filter']"):
                    doc.remove(node)

        if view_type == 'tree':
            partner_string = _('Customer')
            if context.get('type') in ('in_invoice', 'in_refund'):
                partner_string = _('Supplier')
                for node in doc.xpath("//field[@name='reference']"):
                    node.set('invisible', '0')
            for node in doc.xpath("//field[@name='partner_id']"):
                node.set('string', partner_string)

        res['arch'] = etree.tostring(doc)
        return res
    
    def action_move_create(self, cr, uid, ids, context=None):
         
        if self.browse(cr, uid, ids[0], context).is_nomina:
            self.nomina_open(cr, uid, ids, context=None)
             
            for inv in self.browse(cr, uid, ids, context=context):
                
                if not inv.percepcion_line:
                    raise except_orm('No se han agregado percepciones al CFDI', "Por favor agrega al menos una percepcion")
                
                if not inv.deduccion_line:
                    raise except_orm('No se han agregado deducciones al CFDI', "Por favor agrega al menos una deduccion")
                             
                vals_date = self.assigned_datetime(cr, uid,
                    {'invoice_datetime': inv.invoice_datetime,
                        'date_invoice': inv.date_invoice},
                        context=context)
                self.write(cr, uid, ids, vals_date, context=context)            
             
#             return True
         
        res = super(invoice_nomina, self).action_move_create(cr, uid, ids, context=None)
        return res
    
#     def action_move_create(self, cr, uid, ids, context=None):        
#         
#         invoice = self.browse(cr, uid, ids[0], context)
#         
#         if invoice.is_nomina:
#             self.nomina_open(cr, uid, ids, context=None)
#             
#             for inv in self.browse(cr, uid, ids, context=context):
#                                 
#                 vals_date = self.assigned_datetime(cr, uid,
#                     {'invoice_datetime': inv.invoice_datetime,
#                         'date_invoice': inv.date_invoice},
#                         context=context)
#                 self.write(cr, uid, ids, vals_date, context=context)        
#         
#         """Creates invoice related analytics and financial move lines"""
#         ait_obj = self.pool.get('account.invoice.tax')
#         cur_obj = self.pool.get('res.currency')
#         period_obj = self.pool.get('account.period')
#         payment_term_obj = self.pool.get('account.payment.term')
#         journal_obj = self.pool.get('account.journal')
#         move_obj = self.pool.get('account.move')
#         if context is None:
#             context = {}
#         for inv in self.browse(cr, uid, ids, context=context):
#             if not inv.journal_id.sequence_id:
#                 raise osv.except_osv(_('Error!'), _('Please define sequence on the journal related to this invoice.'))
#             if not inv.invoice_line:
#                 raise osv.except_osv(_('No Invoice Lines!'), _('Please create some invoice lines.'))
#             if inv.move_id:
#                 continue
# 
#             ctx = context.copy()
#             ctx.update({'lang': inv.partner_id.lang})
#             if not inv.date_invoice:
#                 self.write(cr, uid, [inv.id], {'date_invoice': fields.date.context_today(self,cr,uid,context=context)}, context=ctx)
#             company_currency = self.pool['res.company'].browse(cr, uid, inv.company_id.id).currency_id.id
#             # create the analytical lines
#             # one move line per invoice line
#             iml = self._get_analytic_lines(cr, uid, inv.id, context=ctx)
#             # check if taxes are all computed
#             compute_taxes = ait_obj.compute(cr, uid, inv.id, context=ctx)
#             self.check_tax_lines(cr, uid, inv, compute_taxes, ait_obj)
# 
#             # I disabled the check_total feature
#             group_check_total_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'account', 'group_supplier_inv_check_total')[1]
#             group_check_total = self.pool.get('res.groups').browse(cr, uid, group_check_total_id, context=context)
#             if group_check_total and uid in [x.id for x in group_check_total.users]:
#                 if (inv.type in ('in_invoice', 'in_refund') and abs(inv.check_total - inv.amount_total) >= (inv.currency_id.rounding/2.0)):
#                     raise osv.except_osv(_('Bad Total!'), _('Please verify the price of the invoice!\nThe encoded total does not match the computed total.'))
# 
#             if inv.payment_term:
#                 total_fixed = total_percent = 0
#                 for line in inv.payment_term.line_ids:
#                     if line.value == 'fixed':
#                         total_fixed += line.value_amount
#                     if line.value == 'procent':
#                         total_percent += line.value_amount
#                 total_fixed = (total_fixed * 100) / (inv.amount_total or 1.0)
#                 if (total_fixed + total_percent) > 100:
#                     raise osv.except_osv(_('Error!'), _("Cannot create the invoice.\nThe related payment term is probably misconfigured as it gives a computed amount greater than the total invoiced amount. In order to avoid rounding issues, the latest line of your payment term must be of type 'balance'."))
# 
#             # one move line per tax line
#             iml += ait_obj.move_line_get(cr, uid, inv.id)
# 
#             entry_type = ''
#             if inv.type in ('in_invoice', 'in_refund'):
#                 ref = inv.reference
#                 entry_type = 'journal_pur_voucher'
#                 if inv.type == 'in_refund':
#                     entry_type = 'cont_voucher'
#             else:
#                 ref = self._convert_ref(cr, uid, inv.number)
#                 entry_type = 'journal_sale_vou'
#                 if inv.type == 'out_refund':
#                     entry_type = 'cont_voucher'
# 
#             diff_currency_p = inv.currency_id.id <> company_currency
#             # create one move line for the total and possibly adjust the other lines amount
#             total = 0
#             total_currency = 0
#             total, total_currency, iml = self.compute_invoice_totals(cr, uid, inv, company_currency, ref, iml, context=ctx)
#             acc_id = inv.account_id.id
# 
#             name = inv['name'] or inv['supplier_invoice_number'] or '/'
#             totlines = False
#             if inv.payment_term:
#                 totlines = payment_term_obj.compute(cr,
#                         uid, inv.payment_term.id, total, inv.date_invoice or False, context=ctx)
#             if totlines:
#                 res_amount_currency = total_currency
#                 i = 0
#                 ctx.update({'date': inv.date_invoice})
#                 for t in totlines:
#                     if inv.currency_id.id != company_currency:
#                         amount_currency = cur_obj.compute(cr, uid, company_currency, inv.currency_id.id, t[1], context=ctx)
#                     else:
#                         amount_currency = False
# 
#                     # last line add the diff
#                     res_amount_currency -= amount_currency or 0
#                     i += 1
#                     if i == len(totlines):
#                         amount_currency += res_amount_currency
# 
#                     iml.append({
#                         'type': 'dest',
#                         'name': name,
#                         'price': t[1],
#                         'account_id': acc_id,
#                         'date_maturity': t[0],
#                         'amount_currency': diff_currency_p \
#                                 and amount_currency or False,
#                         'currency_id': diff_currency_p \
#                                 and inv.currency_id.id or False,
#                         'ref': ref,
#                     })
#             else:
#                 iml.append({
#                     'type': 'dest',
#                     'name': name,
#                     'price': total,
#                     'account_id': acc_id,
#                     'date_maturity': inv.date_due or False,
#                     'amount_currency': diff_currency_p \
#                             and total_currency or False,
#                     'currency_id': diff_currency_p \
#                             and inv.currency_id.id or False,
#                     'ref': ref
#             })
# 
#             date = inv.date_invoice or time.strftime('%Y-%m-%d')
# 
#             part = self.pool.get("res.partner")._find_accounting_partner(inv.partner_id)
# 
#             line = map(lambda x:(0,0,self.line_get_convert(cr, uid, x, part.id, date, context=ctx)),iml)
# 
#             line = self.group_lines(cr, uid, iml, line, inv)
# 
#             journal_id = inv.journal_id.id
#             journal = journal_obj.browse(cr, uid, journal_id, context=ctx)
#             if journal.centralisation:
#                 raise osv.except_osv(_('User Error!'),
#                         _('You cannot create an invoice on a centralized journal. Uncheck the centralized counterpart box in the related journal from the configuration menu.'))
# 
#             line = self.finalize_invoice_move_lines(cr, uid, inv, line)
# 
#             move = {
#                 'ref': inv.reference and inv.reference or inv.name,
#                 'line_id': line,
#                 'journal_id': journal_id,
#                 'date': date,
#                 'narration': inv.comment,
#                 'company_id': inv.company_id.id,
#             }
#             period_id = inv.period_id and inv.period_id.id or False
#             ctx.update(company_id=inv.company_id.id,
#                        account_period_prefer_normal=True)
#             if not period_id:
#                 period_ids = period_obj.find(cr, uid, inv.date_invoice, context=ctx)
#                 period_id = period_ids and period_ids[0] or False
#             if period_id:
#                 move['period_id'] = period_id
#                 for i in line:
#                     i[2]['period_id'] = period_id
# 
#             ctx.update(invoice=inv)
#             move_id = move_obj.create(cr, uid, move, context=ctx)
#             new_move_name = move_obj.browse(cr, uid, move_id, context=ctx).name
#             # make the invoice point to that move
#             self.write(cr, uid, [inv.id], {'move_id': move_id,'period_id':period_id, 'move_name':new_move_name}, context=ctx)
#             # Pass invoice in context in method post: used if you want to get the same
#             # account move reference when creating the same invoice after a cancelled one:
#             if not invoice.is_nomina:            
#                 move_obj.post(cr, uid, [move_id], context=ctx)
#                 
#         self._log_event(cr, uid, ids)
#         return True
    
    def name_get(self, cr, uid, ids, context=None):
        if not ids:
            return []
        types = {
                'out_invoice': _('Invoice'),
                'in_invoice': _('Supplier Invoice'),
                'out_refund': _('Refund'),
                'in_refund': _('Supplier Refund'),
                'nomina_invoice': "Nomina",
                }
        return [(r['id'], '%s %s' % (r['number'] or types[r['type']], r['name'] or '')) for r in self.read(cr, uid, ids, ['type', 'number', 'name'], context, load='_classic_write')]
    
    def create(self, cr, uid, vals, context=None):
        if vals.has_key("is_nomina") and vals["is_nomina"]:
            vals["type"] = "nomina_invoice"
             
        res = super(invoice_nomina, self).create(cr, uid, vals, context)
        return res
    
    @api.multi
    def onchange_partner_id(self, type, partner_id, date_invoice=False,
            payment_term=False, partner_bank_id=False, company_id=False):        
        
        context = self._context
        res = super(invoice_nomina, self).onchange_partner_id(type, partner_id, date_invoice, payment_term, partner_bank_id, company_id)
        
        if context.get("is_nomina"):
            
            journals = self.env['account.journal'].search([("code", "=", "MOM")])
            
            if journals:
                res["value"].update({"journal_id" : journals[0].id})
                
#         if partner_id:
#         
#             partner_obj = self.env["res.partner"]        
#             partner = partner_obj.browse([partner_id])
#         
#             if partner.empleado_id:
#                 res["value"].update({"employee_id" : partner.empleado_id.id})        
            
        return res
    
    @api.multi
    def onchange_employee(self, employee_id):
        
        res = {"value" : {}}
        
        contract_obj = self.env["hr.contract"]        
        employee_obj = self.env["hr.employee"]
        
        employee = employee_obj.browse(employee_id)        
        res["value"].update({"partner_id": employee.address_id.id})
        
        contratos = contract_obj.search([("employee_id", "=", employee_id)])
        if contratos:
            res["value"].update({"contract_id" : contratos[0]})
        
        return res
    
    def onchange_fecha(self, cr, uid, ids, fecha_ini, fecha_fin, context=None):
        
        res = {"value" : {}}
        
        if fecha_ini and fecha_fin: 
            
            if fecha_ini <= fecha_fin:    
                fecha_ini = datetime.strptime(fecha_ini, "%Y-%m-%d")
                fecha_fin = datetime.strptime(fecha_fin, "%Y-%m-%d")
                dias = fecha_fin - fecha_ini
                res["value"].update({"dias_pagados" : dias.days + 1})
            else:
                raise except_orm("Error", "La fecha inicial deberia ser menor a la fecha final del periodo.")
                res["value"].update({"dias_pagados" : None})
        
        return res
    
    def create_ir_attachment_facturae(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        attach = ''
        ir_attach_obj = self.pool.get('ir.attachment.facturae.mx')
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        attach_ids = []
        inv_type_facturae = {
            'out_invoice': True,
            'out_refund': True,
            'nomina_invoice': True,
            'in_invoice': False,
            'in_refund': False}
        for invoice in self.browse(cr, uid, ids, context=context):
            if inv_type_facturae.get(invoice.type, False):
                approval_id = invoice.invoice_sequence_id and invoice.invoice_sequence_id.approval_id or False
                if approval_id:
                    attach_ids.append( ir_attach_obj.create(cr, uid, {
                      'name': invoice.fname_invoice, 'invoice_id': invoice.id,
                      'type': invoice.invoice_sequence_id.approval_id.type},
                      context=None)#Context, because use a variable type of our code but we dont need it.
                    )
        if attach_ids:
            result = mod_obj.get_object_reference(cr, uid, 'l10n_mx_ir_attachment_facturae',
                                                            'action_ir_attachment_facturae_mx')
            id = result and result[1] or False
            result = act_obj.read(cr, uid, [id], context=context)[0]
            #choose the view_mode accordingly
            result['domain'] = "[('id','in',["+','.join(map(str, attach_ids))+"])]"
            result['res_id'] = attach_ids and attach_ids[0] or False
            res = mod_obj.get_object_reference(cr, uid, 'l10n_mx_ir_attachment_facturae', 
                                                            'view_ir_attachment_facturae_mx_form')
            result['views'] = [(res and res[1] or False, 'form')]
            return result
        return True
    
#     def invoice_validate(self, cr, uid, ids, context=None):
#         
#         if self.browse(cr, uid, ids[0], context).is_nomina:
#             self.nomina_open(cr, uid, ids, context=None)
#             
#         res = super(invoice_nomina, self).invoice_validate(cr, uid, ids, context=None)
#         return res
    def _dict_iteritems_sort(self, data_dict):  # cr=False, uid=False, ids=[], context=None):
        """
        @param data_dict : Dictionary with data from invoice
        """
        key_order = [
            'cfdi:Emisor',
            'cfdi:Receptor',
            'cfdi:Conceptos',
            'cfdi:Impuestos',
            'cfdi:Complemento',
            'nomina:Percepciones',
            'nomina:Deducciones',
        ]
        keys = data_dict.keys()
        key_item_sort = []
        for ko in key_order:
            if ko in keys:
                key_item_sort.append([ko, data_dict[ko]])
                keys.pop(keys.index(ko))
        if keys == ['rfc', 'nombre', 'RegimenFiscal', 'DomicilioFiscal', 'ExpedidoEn'] or keys == ['rfc', 'nombre', 'RegimenFiscal', 'ExpedidoEn', 'DomicilioFiscal']:
            keys = ['rfc', 'nombre', 'DomicilioFiscal', 'ExpedidoEn', 'RegimenFiscal']
        if keys ==['rfc', 'nombre', 'cfdi:RegimenFiscal', 'cfdi:DomicilioFiscal', 'cfdi:ExpedidoEn'] or keys == ['rfc', 'nombre', 'cfdi:RegimenFiscal', 'cfdi:ExpedidoEn', 'cfdi:DomicilioFiscal']:
            keys = ['rfc', 'nombre', 'cfdi:DomicilioFiscal', 'cfdi:ExpedidoEn', 'cfdi:RegimenFiscal']
        for key_too in keys:
            key_item_sort.append([key_too, data_dict[key_too]])
        return key_item_sort
    
    def nomina_open(self, cr, uid, ids, context=None):        
        
        nomina = self.browse(cr, uid, ids[0], context)
        percepciones = self.pool.get("account.percepcion.line").search(cr, uid, [("nomina_id","=",ids[0])])
        deducciones = self.pool.get("account.deduccion.line").search(cr, uid, [("nomina_id","=",ids[0])])
#         percepciones = nomina.percepcion_line.ids
#         deducciones = nomina.deduccion_line.ids
        sum_percepciones = 0
        sum_deducciones = 0 
        sum_impuestos = 0 
        
        for per_id in percepciones:
            percepcion = self.pool.get("account.percepcion.line").browse(cr, uid, per_id)
            sum_percepciones = sum_percepciones + percepcion.amount
            
        for ded_id in deducciones:
            deduccion = self.pool.get("account.deduccion.line").browse(cr, uid, ded_id)
            
            if deduccion.cat_id.name == "002":
                sum_impuestos = sum_impuestos + deduccion.amount
                
            else:
                sum_deducciones = sum_deducciones + deduccion.amount
        
        valor_nomina = sum_percepciones - sum_deducciones - sum_impuestos   
        self.write(cr, uid, ids, {
                     "total_percepciones" : sum_percepciones, 
                     "total_deducciones" : sum_deducciones,
                     "total_deducciones_impuestos" : sum_impuestos,
                     "total_nomina" : valor_nomina
                     })
        
        productos = self.pool.get("product.product").search(cr, uid, [("default_code", "=", "NOM")])
        if productos:                    
            product = self.pool.get("product.product").browse(cr, uid, productos[0])
            line_obj = self.pool.get("account.invoice.line")
            line_dict = {
                            "product_id" : product.id,
                            "uos_id" : product.uom_id.id,
                            "name" : "Pago de nomina", 
                            "invoice_id" : ids[0],
                            "price_unit" : sum_percepciones,
                            "price_subtotal" : sum_percepciones,
                        }
            line_obj.create(cr, uid, line_dict)        
        
        return True
    
    def _get_amount_nomina_to_text(self, cr, uid, ids, field_names=None, arg=False, context=None):
        if context is None:
            context = {}
        res = {}
        for invoice in self.browse(cr, uid, ids, context=context):
            amount_to_text = amount_to_text_es_MX.get_amount_to_text(
                self, invoice.total_nomina, 'es_cheque', 'code' in invoice.\
                currency_id._columns and invoice.currency_id.code or invoice.\
                currency_id.name)
            res[invoice.id] = amount_to_text
        return res
    
    _columns = {
        "is_nomina" : fields.boolean("Es Nomina"),
        "fecha_ini" : fields.date("Fecha Inicial"),
        "fecha_fin" : fields.date("Fecha Final"),
        "employee_id" : fields.many2one("hr.employee", "Empleado"),
        "contract_id" : fields.many2one("hr.contract", "Contrato"),
        #rfc_employee" : fields.char("CURP"),
        #"no_employee" : fields.char("No. Empleado"),
        #"curp_employee" : fields.char("CURP"),
        "dias_pagados" : fields.integer("Dias Pagados"),
        #"tipo_regimen" : fields.char("Tipo Regimen"),
        #"periodicidad_pago" : fields.char("Periodicidad Pago"),
        "amount_nomina_to_text":  fields.function(_get_amount_nomina_to_text, method=True,
            type='char', size=256, string='Amount to Text', store=True,
            help='Amount of the invoice in letter'),
        "percepcion_line" : fields.one2many("account.percepcion.line", "nomina_id", "Percepciones Nomina"),
        "deduccion_line" : fields.one2many("account.deduccion.line", "nomina_id", "Deducciones Nomina"),
        "total_percepciones" : fields.float("Percepciones", readonly=True, digits=(12,2)),
        "total_deducciones" : fields.float("Deducciones", readonly=True, digits=(12,2)),
        "total_deducciones_impuestos" : fields.float("Impuestos", readonly=True, digits=(12,2)),
        "total_nomina" : fields.float("Total", readonly=True, digits=(12,2)),
        'type': fields.selection([
            ('out_invoice','Customer Invoice'),
            ('in_invoice','Supplier Invoice'),
            ('out_refund','Customer Refund'),
            ('in_refund','Supplier Refund'),
            ('nomina_invoice','Nomina'),
            ],'Type', readonly=True, select=True, change_default=True, track_visibility='always'),
    }
    
    _defaults = {  
        'is_nomina': _is_nomina,
        'journal_id' : _get_nomina_journal
    }     
    
    def _get_facturae_invoice_xml_data(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        ids = isinstance(ids, (int, long)) and [ids] or ids
        ir_seq_app_obj = self.pool.get('ir.sequence.approval')
        invoice = self.browse(cr, uid, ids[0], context=context)
        sequence_app_id = ir_seq_app_obj.search(cr, uid, [(
            'sequence_id', '=', invoice.invoice_sequence_id.id)], context=context)
        type_inv = 'cfd22'
        if sequence_app_id:
            type_inv = ir_seq_app_obj.browse(
                cr, uid, sequence_app_id[0], context=context).type
        if 'cfdi' in type_inv:
            comprobante = 'cfdi:Comprobante'
            emisor = 'cfdi:Emisor'
            receptor = 'cfdi:Receptor'
            concepto = 'cfdi:Conceptos'
            facturae_version = '3.2'
        else:
            comprobante = 'Comprobante'
            emisor = 'Emisor'
            regimenFiscal = 'RegimenFiscal'
            receptor = 'Receptor'
            concepto = 'Conceptos'
            facturae_version = '2.2'
        
        if invoice.is_nomina:
            data_dict = self._get_facturae_nomina_dict_data(
            cr, uid, ids, context=context)[0]
        else:
            data_dict = self._get_facturae_invoice_dict_data(
                cr, uid, ids, context=context)[0]
                
        doc_xml = self.dict2xml({comprobante: data_dict.get(comprobante)})       
        
        """AGREGAMOS TOTAL DEDUCCIONES Y PERCEPCIONES A LOS NODOS"""
        if invoice.percepcion_line:
                        
            node_Percepciones = doc_xml.getElementsByTagName("nomina:Percepciones")            
            total_gravado = 0.00        
            
            for per in invoice.percepcion_line:
                name = per.name
                cat = per.cat_id
                clave = cat.name
                code = cat.code
                desc = cat.description
                precio = str(per.precio)
                amount = str("%.2f" % per.amount)
                total_gravado = total_gravado + per.amount
        
            node_Percepciones[0].setAttribute("TotalGravado", str("%.2f" % total_gravado))
            node_Percepciones[0].setAttribute("TotalExento", str("0.00"))
        
        if invoice.deduccion_line:
                        
            node_Deducciones = doc_xml.getElementsByTagName("nomina:Deducciones")                        
            total_excento = 0.00
            total_descuento = 0.00
            
            for ded in invoice.deduccion_line:
                name = ded.name
                cat = ded.cat_id
                clave = cat.name
                code = cat.code
                desc = cat.description
                precio = str(ded.precio)
                amount = str("%.2f" % ded.amount)
                total_excento = total_excento + ded.amount
                
            node_Deducciones[0].setAttribute("TotalExento", str("%.2f" % total_gravado))
            node_Deducciones[0].setAttribute("TotalGravado", str("0.00"))  
                            
        invoice_number = "sn"
        (fileno_xml, fname_xml) = tempfile.mkstemp(
            '.xml', 'openerp_' + (invoice_number or '') + '__facturae__')
        fname_txt = fname_xml + '.txt'
        f = open(fname_xml, 'w')
        doc_xml.writexml(
            f, indent='    ', addindent='    ', newl='\r\n', encoding='UTF-8')
        f.close()
        os.close(fileno_xml)
        (fileno_sign, fname_sign) = tempfile.mkstemp('.txt', 'openerp_' + (
            invoice_number or '') + '__facturae_txt_md5__')
        os.close(fileno_sign)
        context.update({
            'fname_xml': fname_xml,
            'fname_txt': fname_txt,
            'fname_sign': fname_sign,
        })
        context.update(self._get_file_globals(cr, uid, ids, context=context))
#         context["fname_xslt"] = "C:\Users\glopzvega\Documents\Github\odoo-mexico\l10n_mx_facturae\SAT\cadenaoriginal_3_2\nomina11.xslt"
        fname_txt, txt_str = self._xml2cad_orig(
            cr=False, uid=False, ids=False, context=context)
        data_dict['cadena_original'] = txt_str
        msg2=''

        if not txt_str:
            raise osv.except_osv(_('Error in Original String!'), _(
                "Can't get the string original of the voucher.\nCkeck your configuration.\n%s" % (msg2)))

        if not data_dict[comprobante].get('folio', ''):
            raise osv.except_osv(_('Error in Folio!'), _(
                "Can't get the folio of the voucher.\nBefore generating the XML, click on the button, generate invoice.\nCkeck your configuration.\n%s" % (msg2)))

        context.update({'fecha': data_dict[comprobante]['fecha']})
        sign_str = self._get_sello(
            cr=False, uid=False, ids=False, context=context)
        if not sign_str:
            raise osv.except_osv(_('Error in Stamp !'), _(
                "Can't generate the stamp of the voucher.\nCkeck your configuration.\ns%s") % (msg2))

        nodeComprobante = doc_xml.getElementsByTagName(comprobante)[0]
        nodeComprobante.setAttribute("sello", sign_str)
        data_dict[comprobante]['sello'] = sign_str

        noCertificado = self._get_noCertificado(cr, uid, ids, context['fname_cer'])
        if not noCertificado:
            raise osv.except_osv(_('Error in No. Certificate !'), _(
                "Can't get the Certificate Number of the voucher.\nCkeck your configuration.\n%s") % (msg2))
        nodeComprobante.setAttribute("noCertificado", noCertificado)
        data_dict[comprobante]['noCertificado'] = noCertificado

        cert_str = self._get_certificate_str(context['fname_cer'])
        if not cert_str:
            raise osv.except_osv(_('Error in Certificate!'), _(
                "Can't get the Certificate Number of the voucher.\nCkeck your configuration.\n%s") % (msg2))
        cert_str = cert_str.replace(' ', '').replace('\n', '')
        nodeComprobante.setAttribute("certificado", cert_str)
        data_dict[comprobante]['certificado'] = cert_str
        if 'cfdi' in type_inv:
            nodeComprobante.removeAttribute('anoAprobacion')
            nodeComprobante.removeAttribute('noAprobacion')
        x = doc_xml.documentElement
        nodeReceptor = doc_xml.getElementsByTagName(receptor)[0]
        nodeConcepto = doc_xml.getElementsByTagName(concepto)[0]
        x.insertBefore(nodeReceptor, nodeConcepto)

        self.write_cfd_data(cr, uid, ids, data_dict, context=context)

        if context.get('type_data') == 'dict':
            return data_dict
        if context.get('type_data') == 'xml_obj':
            return doc_xml
        data_xml = doc_xml.toxml('UTF-8')
        data_xml = codecs.BOM_UTF8 + data_xml
        fname_xml = (data_dict[comprobante][emisor]['rfc'] or '') + '_' + (
            data_dict[comprobante].get('serie', '') or '') + '_' + (
            data_dict[comprobante].get('folio', '') or '') + '.xml'
        data_xml = data_xml.replace(
            '<?xml version="1.0" encoding="UTF-8"?>', '<?xml version="1.0" encoding="UTF-8"?>\n')
        date_invoice = data_dict.get('Comprobante',{}) and datetime.strptime( data_dict.get('Comprobante',{}).get('fecha',{}), '%Y-%m-%dT%H:%M:%S').strftime('%Y-%m-%d') or False
        if date_invoice  and date_invoice < '2012-07-01':
            facturae_version = '2.0'
        self.validate_scheme_facturae_xml(cr, uid, ids, [data_xml], facturae_version)
        data_dict.get('Comprobante',{})
        return fname_xml, data_xml
           
    def _get_facturae_nomina_dict_data(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        ids = isinstance(ids, (int, long)) and [ids] or ids
        datas = self._get_facturae_invoice_dict_data_original(
            cr, uid, ids, context=context)
        ir_seq_app_obj = self.pool.get('ir.sequence.approval')
        invoice = self.browse(cr, uid, ids[0], context=context)
        sequence_app_id = ir_seq_app_obj.search(cr, uid, [(
            'sequence_id', '=', invoice.invoice_sequence_id.id)], context=context)
        type_inv = 'cfd22'
        if sequence_app_id:
            type_inv = ir_seq_app_obj.browse(
                cr, uid, sequence_app_id[0], context=context).type
        for data in datas:
            if 'cfdi' in type_inv:
                comprobante = data['Comprobante']
                rfc = comprobante['Emisor']['rfc']
                nombre = comprobante['Emisor']['nombre']
                dom_Fiscal = comprobante['Emisor']['DomicilioFiscal']
                exp_en = comprobante['Emisor']['ExpedidoEn']
                reg_Fiscal = comprobante['Emisor']['RegimenFiscal']
                rfc_receptor = comprobante['Receptor']['rfc']
                nombre_receptor = comprobante['Receptor']['nombre']
                domicilio_receptor = comprobante['Receptor']['Domicilio']
                totalImpuestosTrasladados = comprobante[
                    'Impuestos']['totalImpuestosTrasladados']
                dict_cfdi_comprobante = {}
                dict_emisor = dict({'rfc': rfc, 'nombre': nombre,
                                    'cfdi:DomicilioFiscal': dom_Fiscal, 'cfdi:ExpedidoEn':
                                    exp_en, 'cfdi:RegimenFiscal': reg_Fiscal})
                dict_receptor = dict({'rfc': rfc_receptor,
                                      'nombre': nombre_receptor, 'cfdi:Domicilio': domicilio_receptor})
                list_conceptos = []
                dict_impuestos = dict({'totalImpuestosTrasladados':
                                       totalImpuestosTrasladados, 'cfdi:Traslados': []})
                totalret = comprobante.get('Impuestos',{}).get('totalImpuestosRetenidos', False)
                if totalret:
                    totalImpuestosRetenidos = comprobante['Impuestos']['totalImpuestosRetenidos']
                    dict_impuestos2 = dict({'totalImpuestosRetenidos':
                                           totalImpuestosRetenidos, 'cfdi:Retenciones': []})
                for concepto in comprobante['Conceptos']:
                    list_conceptos.append(dict({'cfdi:Concepto':
                                                concepto['Concepto']}))
                for traslado in comprobante['Impuestos']['Traslados']:
                    dict_impuestos['cfdi:Traslados'].append(dict(
                        {'cfdi:Traslado': traslado['Traslado']}))
                ret = comprobante.get('Impuestos',{}).get('Retenciones',{})
                if ret:
                    for traslado in ret:
                        dict_impuestos2['cfdi:Retenciones'].append(dict(
                            {'cfdi:Retencion': traslado['Retencion']}))
                comprobante.update({'cfdi:Emisor': dict_emisor,
                                    'cfdi:Receptor': dict_receptor,
                                    'cfdi:Conceptos': list_conceptos,
                                    'cfdi:Impuestos': dict_impuestos,
                                    })
                if ret:
                    comprobante['cfdi:Impuestos'].update(dict_impuestos2)
                
                comprobante.update({"cfdi:Complemento" : {}})
                complemento = comprobante.get("cfdi:Complemento", {})
                complemento.update({"nomina:Nomina" : {}})
                nomina = complemento.get("nomina:Nomina", {})
                nomina.update({
                    "Version" : "1.1",
                    "FechaPago" : invoice.date_invoice,
                    "FechaInicialPago" : invoice.fecha_ini,
                    "FechaFinalPago" : invoice.fecha_fin,
                    "NumDiasPagados" : str(invoice.dias_pagados),                    
                })
                
                if invoice.employee_id:
                    
                    employee = invoice.employee_id
                    
                    nomina.update({
                        "NumEmpleado" : str(employee.no_employee),
                        "Departamento": employee.department_id.name,
                        "Puesto": employee.job_id.name,
                        "CURP": str(employee.identification_id),
                        "NumSeguridadSocial": str(employee.otherid),
                        "RegistroPatronal": employee.patronal
                    })
                    
                contract_obj = self.pool.get("hr.contract")
                contract_ids = contract_obj.get_contract(cr, uid, employee, invoice.fecha_ini, invoice.fecha_fin)
                if contract_ids:
                    contract = contract_obj.browse(cr, uid, contract_ids[0])
                    
                    nomina.update({
                        "SalarioDiarioIntegrado": str("%.2f" % contract.integrated_wage),
                        "TipoRegimen": str(contract.regimen_employee),
                        "PeriodicidadPago": contract.schedule_pay,
                        "FechaInicioRelLaboral": contract.date_start     
                    })
                
                if invoice.percepcion_line:
                    
                    nomina.update({"nomina:Percepciones" : []})
                    percepciones = nomina.get("nomina:Percepciones", [])
                    total_gravado = 0.00                    
                    
                    for per in invoice.percepcion_line:
                        name = per.name
                        cat = per.cat_id
                        clave = cat.name
                        code = cat.code
                        desc = cat.description
                        precio = str(per.precio)
                        amount = str("%.2f" % per.amount)
                        total_gravado = total_gravado + per.amount
                        
                        per_line = { "nomina:Percepcion" : {
                            "TipoPercepcion": code,
                            "Clave": clave,
                            "Concepto": desc,
                            "ImporteGravado": amount,
                            "ImporteExento": "0.00"
                        }}
                        
                        percepciones.append(per_line)
                    
#                     percepciones.update({
#                        "TotalGravado" : str("%.2f" % total_gravado),
#                        "TotalExento": "0.00"                  
#                     });
                
                total_excento = 0.00
                total_descuento = 0.00 
                
                if invoice.deduccion_line:
                    
                    nomina.update({"nomina:Deducciones" : []})
                    deducciones = nomina.get("nomina:Deducciones", [])
                    
                    for ded in invoice.deduccion_line:
                        
                        name = ded.name
                        cat = ded.cat_id
                        clave = cat.name
                        code = cat.code
                        desc = cat.description
                        precio = str(ded.precio)
                        amount = str("%.2f" % ded.amount)
                        total_excento = total_excento + ded.amount
                        
                        if code == "002":
                            
                            impuestos_node = comprobante.get("cfdi:Impuestos", {})
                            impuestos_node.update({"cfdi:Retenciones" : []})
                            retenciones = impuestos_node.get("cfdi:Retenciones", []) 
#                             impuestos_ret_node = impuestos_node.get("cfdi:Retenciones", {})
#                             impuestos_ret_node.update({"cfdi:Retenciones" : {}})
                            
                            
                            ret_line = { "cfdi:Retencion" : {
                                                       "impuesto" : "ISR",
                                                       "importe" : amount
                                                    }}
                            
                            retenciones.append(ret_line)
                            impuestos_node.update({"totalImpuestosRetenidos": str(amount)})
                        
                        else:
                            total_descuento = total_descuento + ded.amount                            
                            
                        ded_line = { "nomina:Deduccion" : {
                            "TipoDeduccion": code,
                            "Clave": clave,
                            "Concepto": desc,
                            "ImporteGravado": "0.00",
                            "ImporteExento": amount
                        }}
                        
                        deducciones.append(ded_line)
                        
#                     deducciones.update({
#                         "TotalExento": str("%.2f" % total_excento),
#                         "TotalGravado" : "0.00"
#                     })
                comprobante.update({
                    "descuento": str("%.2f" % total_descuento),
                    "motivoDescuento": "Deducciones Nomina"
                })
                
                comprobante.pop('Emisor')
                comprobante.pop('Impuestos')
                comprobante.pop('Conceptos')
                comprobante.pop('Receptor')
                
#                 comprobante.pop('xmlns:cfdi')
                comprobante.pop('xsi:schemaLocation')
                
                comprobante.update(                                   
                    {'xmlns:cfdi': "http://www.sat.gob.mx/cfd/3",
                     'xmlns:xsi' : "http://www.w3.org/2001/XMLSchema-instance",
                     'xmlns:nomina' : "http://www.sat.gob.mx/nomina",
                     'xsi:schemaLocation': "http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd http://www.sat.gob.mx/nomina http://www.sat.gob.mx/sitio_internet/cfd/nomina/nomina11.xsd",
                     'version': "3.2", })
                comprobante.pop('xmlns')
                dict_comprobante = comprobante
                data.pop('Comprobante')
                data.update(dict({'cfdi:Comprobante': dict_comprobante}))
        return datas
    
    def _get_facturae_invoice_dict_data_original(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        invoices = self.browse(cr, uid, ids, context=context)
        invoice_tax_obj = self.pool.get("account.invoice.tax")
        invoice_datas = []
        invoice_data_parents = []
        #'type': fields.selection([
            #('out_invoice','Customer Invoice'),
            #('in_invoice','Supplier Invoice'),
            #('out_refund','Customer Refund'),
            #('in_refund','Supplier Refund'),
            #],'Type', readonly=True, select=True),
        for invoice in invoices:
            invoice_data_parent = {}
            if invoice.type == 'out_invoice':
                tipoComprobante = 'ingreso'
            elif invoice.type == 'out_refund':
                tipoComprobante = 'egreso'
            elif invoice.type == 'nomina_invoice':
                tipoComprobante = 'egreso'
            else:
                raise osv.except_osv(_('Warning !'), _(
                    'Only can issue electronic invoice to customers.!'))            
            # Inicia seccion: Comprobante
            invoice_data_parent['Comprobante'] = {}
            # default data
            invoice_data_parent['Comprobante'].update({
                'xmlns': "http://www.sat.gob.mx/cfd/2",
                'xmlns:xsi': "http://www.w3.org/2001/XMLSchema-instance",
                'xsi:schemaLocation': "http://www.sat.gob.mx/cfd/2 http://www.\
                sat.gob.mx/sitio_internet/cfd/2/cfdv2.xsd",
                'version': "2.0",
            })
            number_work = invoice.number or invoice.internal_number
            invoice_data_parent['Comprobante'].update({
                'folio': number_work,
                'fecha': invoice.date_invoice_tz and
                # time.strftime('%d/%m/%y',
                # time.strptime(invoice.date_invoice, '%Y-%m-%d'))
                time.strftime('%Y-%m-%dT%H:%M:%S', time.strptime(
                invoice.date_invoice_tz, '%Y-%m-%d %H:%M:%S'))
                or '',
                'tipoDeComprobante': tipoComprobante,
                'formaDePago': u'Pago en una sola exhibición',
                'noCertificado': '@',
                'sello': '@',
                'certificado': '@',
                'subTotal': "%.2f" % (invoice.amount_untaxed or 0.0),
                'descuento': "0",  # Add field general
                'total': "%.2f" % (invoice.amount_total or 0.0),
            })
            folio_data = self._get_folio(
                cr, uid, [invoice.id], context=context)
            invoice_data_parent['Comprobante'].update({
                'anoAprobacion': folio_data['anoAprobacion'],
                'noAprobacion': folio_data['noAprobacion'],
            })
            serie = folio_data.get('serie', False)
            if serie:
                invoice_data_parent['Comprobante'].update({
                    'serie': serie,
                })
            # Termina seccion: Comprobante
            # Inicia seccion: Emisor
            partner_obj = self.pool.get('res.partner')
            address_invoice = invoice.address_issued_id or False
            address_invoice_parent = invoice.company_emitter_id and \
            invoice.company_emitter_id.address_invoice_parent_company_id or False

            if not address_invoice:
                raise osv.except_osv(_('Warning !'), _(
                    "Don't have defined the address issuing!"))

            if not address_invoice_parent:
                raise osv.except_osv(_('Warning !'), _(
                    "Don't have defined an address of invoicing from the company!"))

            if not address_invoice_parent.vat:
                raise osv.except_osv(_('Warning !'), _(
                    "Don't have defined RFC for the address of invoice to the company!"))

            invoice_data = invoice_data_parent['Comprobante']
            invoice_data['Emisor'] = {}
            invoice_data['Emisor'].update({

                'rfc': (('vat_split' in address_invoice_parent._columns and \
                address_invoice_parent.vat_split or address_invoice_parent.vat) \
                or '').replace('-', ' ').replace(' ', '').upper(),
                'nombre': address_invoice_parent.name or '',
                # Obtener domicilio dinamicamente
                # virtual_invoice.append( (invoice.company_id and
                # invoice.company_id.partner_id and
                # invoice.company_id.partner_id.vat or '').replac

                'DomicilioFiscal': {
                    'calle': address_invoice_parent.street and \
                        address_invoice_parent.street.replace('\n\r', ' ').\
                        replace('\r\n', ' ').replace('\n', ' ').replace(
                        '\r', ' ') or '',
                    'noExterior': address_invoice_parent.l10n_mx_street3 and \
                        address_invoice_parent.l10n_mx_street3.replace(
                        '\n\r', ' ').replace('\r\n', ' ').replace('\n', ' ').\
                        replace('\r', ' ') or 'N/A',  # "Numero Exterior"
                    'noInterior': address_invoice_parent.l10n_mx_street4 and \
                        address_invoice_parent.l10n_mx_street4.replace(
                        '\n\r', ' ').replace('\r\n', ' ').replace('\n', ' ').\
                        replace('\r', ' ') or 'N/A',  # "Numero Interior"
                    'colonia':  address_invoice_parent.street2 and \
                        address_invoice_parent.street2.replace('\n\r', ' ').\
                        replace('\r\n', ' ').replace('\n', ' ').replace(
                        '\r', ' ') or False,
                    'localidad': address_invoice_parent.l10n_mx_city2 and \
                        address_invoice_parent.l10n_mx_city2.replace(
                        '\n\r', ' ').replace('\r\n', ' ').replace('\n', ' ').\
                        replace('\r', ' ') or False,
                    'municipio': address_invoice_parent.city and \
                        address_invoice_parent.city.replace('\n\r', ' ').\
                        replace('\r\n', ' ').replace('\n', ' ').replace(
                        '\r', ' ') or '',
                    'estado': address_invoice_parent.state_id and \
                        address_invoice_parent.state_id.name and \
                        address_invoice_parent.state_id.name.replace(
                        '\n\r', ' ').replace('\r\n', ' ').replace(
                        '\n', ' ').replace('\r', ' ') or '',
                    'pais': address_invoice_parent.country_id and \
                        address_invoice_parent.country_id.name and \
                        address_invoice_parent.country_id.name.replace(
                        '\n\r', ' ').replace('\r\n', ' ').replace(
                        '\n', ' ').replace('\r', ' ')or '',
                    'codigoPostal': address_invoice_parent.zip and \
                        address_invoice_parent.zip.replace('\n\r', ' ').\
                        replace('\r\n', ' ').replace('\n', ' ').replace(
                        '\r', ' ').replace(' ', '') or '',
                },
                'ExpedidoEn': {
                    'calle': address_invoice.street and address_invoice.\
                        street.replace('\n\r', ' ').replace('\r\n', ' ').\
                        replace('\n', ' ').replace('\r', ' ') or '',
                    'noExterior': address_invoice.l10n_mx_street3 and \
                        address_invoice.l10n_mx_street3.replace(
                        '\n\r', ' ').replace('\r\n', ' ').replace('\n', ' ').\
                        replace('\r', ' ') or 'N/A',  # "Numero Exterior"
                    'noInterior': address_invoice.l10n_mx_street4 and \
                        address_invoice.l10n_mx_street4.replace('\n\r', ' ').\
                        replace('\r\n', ' ').replace('\n', ' ').replace(
                        '\r', ' ') or 'N/A',  # "Numero Interior"
                    'colonia':  address_invoice.street2 and address_invoice.\
                        street2.replace('\n\r', ' ').replace('\r\n', ' ').\
                        replace('\n', ' ').replace('\r', ' ') or False,
                    'localidad': address_invoice.l10n_mx_city2 and \
                        address_invoice.l10n_mx_city2.replace('\n\r', ' ').\
                        replace('\r\n', ' ').replace('\n', ' ').replace(
                        '\r', ' ') or False,
                    'municipio': address_invoice.city and address_invoice.\
                        city.replace('\n\r', ' ').replace('\r\n', ' ').replace(
                        '\n', ' ').replace('\r', ' ') or '',
                    'estado': address_invoice.state_id and address_invoice.\
                        state_id.name and address_invoice.state_id.name.replace(
                        '\n\r', ' ').replace('\r\n', ' ').replace('\n', ' ').\
                        replace('\r', ' ') or '',
                    'pais': address_invoice.country_id and address_invoice.\
                        country_id.name and address_invoice.country_id.name.\
                        replace('\n\r', ' ').replace('\r\n', ' ').replace(
                        '\n', ' ').replace('\r', ' ')or '',
                    'codigoPostal': address_invoice.zip and address_invoice.\
                        zip.replace('\n\r', ' ').replace('\r\n', ' ').replace(
                        '\n', ' ').replace('\r', ' ').replace(' ', '') or '',
                },
            })
            if invoice_data['Emisor']['DomicilioFiscal'].get('colonia') == False:
                invoice_data['Emisor']['DomicilioFiscal'].pop('colonia')
            if invoice_data['Emisor']['ExpedidoEn'].get('colonia') == False:
                invoice_data['Emisor']['ExpedidoEn'].pop('colonia')
            if invoice_data['Emisor']['DomicilioFiscal'].get('localidad') == False:
                invoice_data['Emisor']['DomicilioFiscal'].pop('localidad')
            if invoice_data['Emisor']['ExpedidoEn'].get('localidad') == False:
                invoice_data['Emisor']['ExpedidoEn'].pop('localidad')
            # Termina seccion: Emisor
            # Inicia seccion: Receptor
            parent_id = invoice.partner_id.commercial_partner_id.id
            parent_obj = partner_obj.browse(cr, uid, parent_id, context=context)
            if not parent_obj.vat:
                msg2 = "Contact you administrator &/or to info@vauxoo.com"
                raise osv.except_osv(_('Warning !'), _(
                    "Don't have defined RFC of the partner[%s].\n%s !") % (
                    parent_obj.name, msg2))
            if parent_obj._columns.has_key('vat_split') and parent_obj.vat[0:2].upper() <> 'MX':
                    rfc = 'XAXX010101000'
            else:
                rfc = ((parent_obj._columns.has_key('vat_split')\
                    and parent_obj.vat_split or parent_obj.vat)\
                    or '').replace('-', ' ').replace(' ','').upper()
            address_invoice = partner_obj.browse(cr, uid, \
                invoice.partner_id.id, context=context)
            invoice_data['Receptor'] = {}
            invoice_data['Receptor'].update({
                'rfc': rfc.upper(),
                'nombre': (parent_obj.name or ''),
                'Domicilio': {
                    'calle': address_invoice.street and address_invoice.\
                        street.replace('\n\r', ' ').replace('\r\n', ' ').replace(
                        '\n', ' ').replace('\r', ' ') or '',
                    'noExterior': address_invoice.l10n_mx_street3 and \
                        address_invoice.l10n_mx_street3.replace('\n\r', ' ').\
                        replace('\r\n', ' ').replace('\n', ' ').replace(
                        '\r', ' ') or 'N/A',  # "Numero Exterior"
                    'noInterior': address_invoice.l10n_mx_street4 and \
                        address_invoice.l10n_mx_street4.replace('\n\r', ' ').\
                        replace('\r\n', ' ').replace('\n', ' ').replace(
                        '\r', ' ') or 'N/A',  # "Numero Interior"
                    'colonia':  address_invoice.street2 and address_invoice.\
                        street2.replace('\n\r', ' ').replace('\r\n', ' ').\
                        replace('\n', ' ').replace('\r', ' ') or False,
                    'localidad': address_invoice.l10n_mx_city2 and \
                        address_invoice.l10n_mx_city2.replace('\n\r', ' ').\
                        replace('\r\n', ' ').replace('\n', ' ').replace(
                        '\r', ' ') or False,
                    'municipio': address_invoice.city and address_invoice.\
                        city.replace('\n\r', ' ').replace('\r\n', ' ').replace(
                        '\n', ' ').replace('\r', ' ') or '',
                    'estado': address_invoice.state_id and address_invoice.\
                        state_id.name and address_invoice.state_id.name.replace(
                        '\n\r', ' ').replace('\r\n', ' ').replace('\n', ' ').\
                        replace('\r', ' ') or '',
                    'pais': address_invoice.country_id and address_invoice.\
                        country_id.name and address_invoice.country_id.name.\
                        replace('\n\r', ' ').replace('\r\n', ' ').replace(
                        '\n', ' ').replace('\r', ' ')or '',
                    'codigoPostal': address_invoice.zip and address_invoice.\
                        zip.replace('\n\r', ' ').replace('\r\n', ' ').replace(
                        '\n', ' ').replace('\r', ' ') or '',
                },
            })
            if invoice_data['Receptor']['Domicilio'].get('colonia') == False:
                invoice_data['Receptor']['Domicilio'].pop('colonia')
            if invoice_data['Receptor']['Domicilio'].get('localidad') == False:
                invoice_data['Receptor']['Domicilio'].pop('localidad')
            # Termina seccion: Receptor
            # Inicia seccion: Conceptos
            invoice_data['Conceptos'] = []
            for line in invoice.invoice_line:
                # price_type = invoice._columns.has_key('price_type') and invoice.price_type or 'tax_excluded'
                # if price_type == 'tax_included':
# price_unit = line.price_subtotal/line.quantity#Agrega compatibilidad con
# modulo TaxIncluded
                price_unit = line.quantity != 0 and line.price_subtotal / \
                    line.quantity or 0.0
                concepto = {
                    'cantidad': "%.2f" % (line.quantity or 0.0),
                    'descripcion': line.name or '',
                    'valorUnitario': "%.2f" % (price_unit or 0.0),
                    'importe': "%.2f" % (line.price_subtotal or 0.0),  # round(line.price_unit *(1-(line.discount/100)),2) or 0.00),#Calc: iva, disc, qty
                    # Falta agregar discount
                }
                unidad = line.uos_id and line.uos_id.name or ''
                if unidad:
                    concepto.update({'unidad': unidad})
                product_code = line.product_id and line.product_id.default_code or ''
                if product_code:
                    concepto.update({'noIdentificacion': product_code})
                invoice_data['Conceptos'].append({'Concepto': concepto})

                pedimento = None
                if 'tracking_id' in line._columns:
                    pedimento = line.tracking_id and line.tracking_id.import_id or False
                    if pedimento:
                        informacion_aduanera = {
                            'numero': pedimento.name or '',
                            'fecha': pedimento.date or '',
                            'aduana': pedimento.customs,
                        }
                        concepto.update({
                                        'InformacionAduanera': informacion_aduanera})
                # Termina seccion: Conceptos
            # Inicia seccion: impuestos
            invoice_data['Impuestos'] = {}
            invoice_data['Impuestos'].update({
                #'totalImpuestosTrasladados': "%.2f"%( invoice.amount_tax or 0.0),
            })
            invoice_data['Impuestos'].update({
                #'totalImpuestosRetenidos': "%.2f"%( invoice.amount_tax or 0.0 )
            })

            invoice_data_impuestos = invoice_data['Impuestos']
            invoice_data_impuestos['Traslados'] = []
            # invoice_data_impuestos['Retenciones'] = []

            tax_names = []
            totalImpuestosTrasladados = 0
            totalImpuestosRetenidos = 0
            for line_tax_id in invoice.tax_line:
                tax_name = line_tax_id.name2
                tax_names.append(tax_name)
                line_tax_id_amount = abs(line_tax_id.amount or 0.0)
                if line_tax_id.amount >= 0:
                    impuesto_list = invoice_data_impuestos['Traslados']
                    impuesto_str = 'Traslado'
                    totalImpuestosTrasladados += line_tax_id_amount
                else:
                    # impuesto_list = invoice_data_impuestos['Retenciones']
                    impuesto_list = invoice_data_impuestos.setdefault(
                        'Retenciones', [])
                    impuesto_str = 'Retencion'
                    totalImpuestosRetenidos += line_tax_id_amount
                    invoice_data['Impuestos'].update({
                                    'totalImpuestosRetenidos': "%.2f" % (totalImpuestosRetenidos)
                                    })
                impuesto_dict = {impuesto_str:
                                {
                                 'impuesto': tax_name,
                                 'importe': "%.2f" % (line_tax_id_amount),
                                 }
                                 }
                if line_tax_id.amount >= 0:
                    impuesto_dict[impuesto_str].update({
                            'tasa': "%.2f" % (abs(line_tax_id.tax_percent))})
                impuesto_list.append(impuesto_dict)
            invoice_data['Impuestos'].update({
                'totalImpuestosTrasladados': "%.2f" % (totalImpuestosTrasladados),
            })
            tax_requireds = ['IVA', 'IEPS']
            for tax_required in tax_requireds:
                if tax_required in tax_names:
                    continue
                invoice_data_impuestos['Traslados'].append({'Traslado': {
                    'impuesto': tax_required,
                    'tasa': "%.2f" % (0.0),
                    'importe': "%.2f" % (0.0),
                }})
            # Termina seccion: impuestos
            invoice_data_parents.append(invoice_data_parent)
            invoice_data_parent['state'] = invoice.state
            invoice_data_parent['invoice_id'] = invoice.id
            invoice_data_parent['type'] = invoice.type
            invoice_data_parent['invoice_datetime'] = invoice.invoice_datetime
            invoice_data_parent['date_invoice_tz'] = invoice.date_invoice_tz
            invoice_data_parent['currency_id'] = invoice.currency_id.id

            date_ctx = {'date': invoice.date_invoice_tz and time.strftime(
                '%Y-%m-%d', time.strptime(invoice.date_invoice_tz,
                '%Y-%m-%d %H:%M:%S')) or False}
            # rate = self.pool.get('res.currency').compute(cr, uid, invoice.currency_id.id, invoice.company_id.currency_id.id, 1, round=False, context=date_ctx, account=None, account_invert=False)
            # rate = 1.0/self.pool.get('res.currency')._current_rate(cr, uid,
            # [invoice.currency_id.id], name=False, arg=[],
            # context=date_ctx)[invoice.currency_id.id]
            currency = self.pool.get('res.currency').browse(
                cr, uid, [invoice.currency_id.id], context=date_ctx)[0]
            rate = currency.rate != 0 and 1.0/currency.rate or 0.0
            # print "currency.rate",currency.rate

            invoice_data_parent['rate'] = rate

        invoice_datetime = invoice_data_parents[0].get('invoice_datetime',
            {}) and datetime.strptime(invoice_data_parents[0].get(
            'invoice_datetime', {}), '%Y-%m-%d %H:%M:%S').strftime(
            '%Y-%m-%d') or False
        if not invoice_datetime:
            raise osv.except_osv(_('Date Invoice Empty'), _(
                "Can't generate a invoice without date, make sure that the state of invoice not is draft & the date of invoice not is empty"))
        if invoice_datetime < '2012-07-01':
            return invoice_data_parent
        else:
            invoice = self.browse(cr, uid, ids, context={
                                  'date': invoice_datetime})[0]
            city = invoice_data_parents and invoice_data_parents[0].get(
                'Comprobante', {}).get('Emisor', {}).get('ExpedidoEn', {}).get(
                'municipio', {}) or False
            state = invoice_data_parents and invoice_data_parents[0].get(
                'Comprobante', {}).get('Emisor', {}).get('ExpedidoEn', {}).get(
                'estado', {}) or False
            country = invoice_data_parents and invoice_data_parents[0].get(
                'Comprobante', {}).get('Emisor', {}).get('ExpedidoEn', {}).get(
                'pais', {}) or False
            if city and state and country:
                address = city + ' ' + state + ', ' + country
            else:
                raise osv.except_osv(_('Address Incomplete!'), _(
                    'Ckeck that the address of company issuing of fiscal voucher is complete (City - State - Country)'))

            if not invoice.company_emitter_id.partner_id.regimen_fiscal_id.name:
                raise osv.except_osv(_('Missing Fiscal Regime!'), _(
                    'The Fiscal Regime of the company issuing of fiscal voucher is a data required'))

            invoice_data_parents[0]['Comprobante'][
                'xsi:schemaLocation'] = 'http://www.sat.gob.mx/cfd/2 http://www.sat.gob.mx/sitio_internet/cfd/2/cfdv22.xsd'
            invoice_data_parents[0]['Comprobante']['version'] = '2.2'
            invoice_data_parents[0]['Comprobante'][
                'TipoCambio'] = invoice.rate or 1
            invoice_data_parents[0]['Comprobante'][
                'Moneda'] = invoice.currency_id.name or ''
            invoice_data_parents[0]['Comprobante'][
                'NumCtaPago'] = invoice.acc_payment.last_acc_number\
                    or 'No identificado'
            invoice_data_parents[0]['Comprobante'][
                'metodoDePago'] = invoice.pay_method_id.name or 'No identificado'
            invoice_data_parents[0]['Comprobante']['Emisor']['RegimenFiscal'] = {
                'Regimen': invoice.company_emitter_id.partner_id.\
                    regimen_fiscal_id.name or ''}
            invoice_data_parents[0]['Comprobante']['LugarExpedicion'] = address
        return invoice_data_parents
    
invoice_nomina()


class invoice_percepcion_line(osv.osv):
    _name = "account.percepcion.line"
 
    _columns = {
        "name" : fields.char("Descripcion"),
        "cat_id" : fields.many2one("io.cat.sat", "Codigo"),
        "precio" : fields.float("Precio", digits=(2,0)),
        "amount" : fields.float("Subtotal", digits=(2,0)), 
        "nomina_id" : fields.many2one("account.invoice", "Nomina")         
    }
 
     
invoice_percepcion_line()
 
class invoice_deduccion_line(osv.osv):
    _name = "account.deduccion.line"
     
    _columns = {
        "name" : fields.char("Descripcion"),
        "cat_id" : fields.many2one("io.cat.sat", "Codigo"),
        "precio" : fields.float("Precio", digits=(2,0)),
        "amount" : fields.float("Subtotal", digits=(2,0)),    
        "nomina_id" : fields.many2one("account.invoice", "Nomina")    
    }

