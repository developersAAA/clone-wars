# -*- encoding: utf-8 -*-

from openerp.osv import fields, osv
from openerp.tools.translate import _

class params_pac(osv.Model):
    _inherit = 'params.pac'

    def _get_method_type_selection(self, cr, uid, context=None):
        types = super(params_pac, self)._get_method_type_selection(
            cr, uid, context=context)
        types.extend([
            ('pac_fk_cancelar', 'PAC FK - Cancel'),
            ('pac_fk_firmar', 'PAC FK - Firmar'),
        ])
        return types    
    
    _columns = {
        'method_type': fields.selection(_get_method_type_selection,
                                        "Process to perform", type='char', size=64, required=True,
                                        help='Type of process to configure in this pac'),
    }
