# -*- coding: utf-8 -*-
from openerp.osv import osv, fields

class res_partner_empleado(osv.osv):
    _inherit = "res.partner"
    
    _columns = {
        "empleado" : fields.boolean("Es Empleado"),
        "empleado_id" : fields.many2one("hr.employee", "Empleado"),   
        
    }
    
res_partner_empleado()