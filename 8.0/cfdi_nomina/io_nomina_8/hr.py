# -*- coding: utf-8 -*-
from openerp.osv import osv, fields

class hr_employee(osv.osv):
    _inherit = "hr.employee"
    
    _columns = {                
        "patronal" : fields.char("Registro Patronal"),
    }
    
hr_employee()

class hr_contract(osv.osv):
    _inherit = "hr.contract"
    
    def get_contract(self, cr, uid, employee, date_from, date_to, context=None):
        """
        @param employee: browse record of employee
        @param date_from: date field
        @param date_to: date field
        @return: returns the ids of all the contracts for the given employee that need to be considered for the given dates
        """
        contract_obj = self.pool.get('hr.contract')
        clause = []
        #a contract is valid if it ends between the given dates
        clause_1 = ['&',('date_end', '<=', date_to),('date_end','>=', date_from)]
        #OR if it starts between the given dates
        clause_2 = ['&',('date_start', '<=', date_to),('date_start','>=', date_from)]
        #OR if it starts before the date_from and finish after the date_end (or never finish)
        clause_3 = ['&',('date_start','<=', date_from),'|',('date_end', '=', False),('date_end','>=', date_to)]
        clause_final =  [('employee_id', '=', employee.id),'|','|'] + clause_1 + clause_2 + clause_3
        contract_ids = contract_obj.search(cr, uid, clause_final, context=context)
        return contract_ids
    
    _columns = {
                                        
        "schedule_pay" : fields.selection([
            ('Diario', 'Diario'),
            ('Semanal', 'Semanal'),
            ('Quincenal', 'Quincenal'),
            ('Decenal', 'Decenal'),
            ('Catorcena Mensual', 'Catorcena Mensual'),
            ('Mensual', 'Mensual'),
            ('Bimestral', 'Bimestral'),
            ('Unidad de Obra', 'Unidad de Obra'),
            ('Comisión', 'Comisión'),
            ('Precio Alzado', 'Precio Alzado'),
            ], 'Periodo de Pago', select=True),       
    }
    
    _defaults = {  
        'schedule_pay': "Quincenal",  
        }
    
hr_contract()