# -*- encoding: utf-8 -*-
import StringIO
import base64
import codecs
import csv
from datetime import datetime, timedelta
import logging
from openerp import api
from openerp import netsvc
from openerp import pooler
from openerp import tools
from openerp import tools
import os
from pytz import timezone
import pytz
from suds.client import Client
import sys
import tempfile
import time
import time
import urllib
from xml.dom import minidom
import xml.dom.minidom
import traceback

from openerp.osv import fields, osv, orm
from openerp.tools.misc import ustr
from openerp.tools.translate import _
from openerp.tools.translate import _

import qrcode

_logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)
logging.getLogger("suds.client").setLevel(logging.DEBUG)
logging.getLogger("suds.transport").setLevel(logging.DEBUG)
logging.getLogger("suds.xsd.schema").setLevel(logging.DEBUG)
logging.getLogger("suds.wsdl").setLevel(logging.DEBUG)

try:
    from SOAPpy import WSDL
except:
    _logger.warning('Install Package SOAPpy with the command "sudo apt-get install python-soappy".')

class ir_attachment_facturae_mx(osv.Model):
    _inherit = 'ir.attachment.facturae.mx'  
    
    def _get_type(self, cr, uid, ids=None, context=None):
        if context is None:
            context = {}
        types = []
        types.extend([
            ('cfdi32_pac_sf', 'CFDI 3.2 PAC'),
        ])        
        return types
    
    def get_driver_fc_sign(self):
        factura_mx_type__fc = super(ir_attachment_facturae_mx, self).get_driver_fc_sign()
        if factura_mx_type__fc == None:
            factura_mx_type__fc = {}
        factura_mx_type__fc.update({'cfdi32_pac_sf': self._upload_ws_file})
        return factura_mx_type__fc
    
    def get_driver_fc_cancel(self):
        factura_mx_type__fc = super(ir_attachment_facturae_mx, self).get_driver_fc_cancel()
        if factura_mx_type__fc == None:
            factura_mx_type__fc = {}
        factura_mx_type__fc.update({'cfdi32_pac_sf': self.sf_cancel})
        return factura_mx_type__fc
        
    _columns = {
        'state': fields.selection([
            ('draft', 'Borrador'),
            ('confirmed', 'Confirmada'),
            ('signed', 'Firmada'),            
            ('cancel', 'Cancelada')],
            'Estatus', readonly=True, required=True, help='Estado del Archivo'),
        'type': fields.selection(_get_type, 'Type', type='char', size=64,
                                 required=True, readonly=True, help="Type of Electronic Invoice"),
    }
    
    def signal_confirm(self, cr, uid, ids, context=None):
        if context is None:
            context = {}   
            
        from openerp.addons.l10n_mx_facturae_lib import facturae_lib
        msj, app_xsltproc_fullpath, app_openssl_fullpath, app_xmlstarlet_fullpath = facturae_lib.library_openssl_xsltproc_xmlstarlet(cr, uid, ids, context)
        if msj:
            raise osv.except_osv(_('Warning'),_(msj))
        try:
            if context is None:
                context = {}
            ids = isinstance(ids, (int, long)) and [ids] or ids
            invoice_obj = self.pool.get('account.invoice')
            attach = ''
            msj = ''
            index_xml = ''
            attach = self.browse(cr, uid, ids[0])
            invoice = attach.invoice_id
            type = attach.type
            wf_service = netsvc.LocalService("workflow")
            save_attach = None
            if 'cbb' in type:
                msj = _("Confirmed")
                save_attach = False
            elif 'cfdi' in type:
                fname_invoice = invoice.fname_invoice and invoice.fname_invoice + \
                    '_V3_2.xml' or ''
                fname, xml_data = invoice_obj._get_facturae_invoice_xml_data(
                    cr, uid, [invoice.id], context=context)                
                               
#                 if not invoice.is_nomina:
#                     xml_res_str = xml.dom.minidom.parseString(xml_data)
#                     xml_data = invoice_obj.add_nomina_xml(cr, uid, xml_res_str, invoice, context=context)
#                     xml_data = xml_data.toxml('UTF-8')
#                     xml_data = xml_data.replace(codecs.BOM_UTF8, '')
                  
                attach = self.pool.get('ir.attachment').create(cr, uid, {
                    'name': fname_invoice,
                    'datas': base64.encodestring(xml_data),
                    'datas_fname': fname_invoice,
                    'res_model': 'account.invoice',
                    'res_id': invoice.id,
                }, context=None)
                msj = _("Attached Successfully XML CFDI 3.2\n")
                save_attach = True
            elif 'cfd' in type and not 'cfdi' in type:
                fname_invoice = invoice.fname_invoice and invoice.fname_invoice + \
                    '.xml' or ''
                fname, xml_data = invoice_obj._get_facturae_invoice_xml_data(
                    cr, uid, [invoice.id], context=context)
                attach = self.pool.get('ir.attachment').create(cr, uid, {
                    'name': fname_invoice,
                    'datas': base64.encodestring(xml_data),
                    'datas_fname': fname_invoice,
                    'res_model': 'account.invoice',
                    'res_id': invoice.id,
                }, context=None)
                if attach:
                    index_xml = self.pool.get('ir.attachment').browse(
                        cr, uid, attach).index_content
                    msj = _("Attached Successfully XML CFD 2.2")
                save_attach = True
            else:
                raise osv.except_osv(_("Type Electronic Invoice Unknow!"), _(
                    "The Type Electronic Invoice:" + (type or '')))
            if save_attach:
                self.write(cr, uid, ids,
                           {'file_input': attach or False,
                               'last_date': time.strftime('%Y-%m-%d %H:%M:%S'),
                               'msj': msj,
                               'file_xml_sign_index': index_xml}, context=context)
            wf_service.trg_validate(
                uid, self._name, ids[0], 'action_confirm', cr)
            return True
        except Exception, e:
            error = tools.ustr(traceback.format_exc())
            self.write(cr, uid, ids, {'msj': error}, context=context)
            _logger.error(error)
            return False   
    
    
    def _upload_ws_file(self, cr, uid, ids, fdata=None, context=None):
        """
        @params fdata : File.xml codification in base64
        """
        if context is None:
            context = {}
        invoice_obj = self.pool.get('account.invoice')
        pac_params_obj = invoice_obj.pool.get('params.pac')
        for ir_attachment_facturae_mx_id in self.browse(cr, uid, ids, context=context):
            invoice = ir_attachment_facturae_mx_id.invoice_id
            comprobante = invoice_obj._get_type_sequence(
                cr, uid, [invoice.id], context=context)
            cfd_data = base64.decodestring(fdata or invoice_obj.fdata)
            xml_res_str = xml.dom.minidom.parseString(cfd_data)
            xml_res_addenda = invoice_obj.add_addenta_xml(
                 cr, uid, xml_res_str, comprobante, context=context)
#             if invoice.is_nomina:
#                 xml_res_addenda = invoice_obj.add_nomina_xml(
#                      cr, uid, xml_res_addenda, invoice, context=context)
            xml_res_str_addenda = xml_res_addenda.toxml('UTF-8')
            xml_res_str_addenda = xml_res_str_addenda.replace(codecs.BOM_UTF8, '')
#             
            if tools.config['test_report_directory']:#TODO: Add if test-enabled:
                ir_attach_facturae_mx_file_input = ir_attachment_facturae_mx_id.file_input and ir_attachment_facturae_mx_id.file_input or False
                fname_suffix = ir_attach_facturae_mx_file_input and ir_attach_facturae_mx_file_input.datas_fname or ''
                open( os.path.join(tools.config['test_report_directory'], 'l10n_mx_facturae_pac_sf' + '_' + \
                'before_upload' + '-' + fname_suffix), 'wb+').write( xml_res_str_addenda )
            compr = xml_res_addenda.getElementsByTagName(comprobante)[0]
            date = compr.attributes['fecha'].value
            date_format = datetime.strptime(
                date, '%Y-%m-%dT%H:%M:%S').strftime('%Y-%m-%d')
            context['date'] = date_format
            invoice_ids = [invoice.id]
            file = False
            msg = ''
            cfdi_xml = False
            
            if invoice.is_nomina:
                method = 'pac_fk_firmar'
            else:
                method = 'pac_sf_firmar'
            
            pac_params_ids = pac_params_obj.search(cr, uid, [
                ('method_type', '=', method), (
                    'company_id', '=', invoice.company_emitter_id.id), (
                        'active', '=', True)], limit=1, context=context)
            
            if pac_params_ids:
                pac_params = pac_params_obj.browse(
                    cr, uid, pac_params_ids, context)[0]
                user = pac_params.user
                password = pac_params.password
                wsdl_url = pac_params.url_webservice
                namespace = pac_params.namespace
                
                if invoice.is_nomina:
                    url = 'https://facturacion.finkok.com/servicios/soap/stamp.wsdl'
                    testing_url = 'http://demo-facturacion.finkok.com/servicios/soap/stamp.wsdl'
                    namespace_text = 'http://facturacion.finkok.com'
                else:
                    url = 'https://solucionfactible.com/ws/services/Timbrado'
                    testing_url = 'http://testing.solucionfactible.com/ws/services/Timbrado'
                    namespace_text = 'http://timbrado.ws.cfdi.solucionfactible.com'
                    
                if (wsdl_url == url) or (wsdl_url == testing_url):
                    pass
                else:
                    raise osv.except_osv(_('Warning'), _('Web Service URL o PAC incorrect'))
                
                if namespace == namespace_text:
                    pass
                else:
                    raise osv.except_osv(_('Warning'), _('Namespace of PAC incorrect'))
                if 'demo' in wsdl_url:
                    msg += _(u'WARNING, SIGNED IN TEST!!!!\n\n')
                
                if invoice.is_nomina:
                    wsdl_client = Client(wsdl_url, cache=None)
                else:
                    wsdl_client = WSDL.SOAPProxy(wsdl_url, namespace)                
                
                file_globals = invoice_obj._get_file_globals(cr, uid, invoice_ids, context=context)
                fname_cer_no_pem = file_globals['fname_cer']
                cerCSD = fname_cer_no_pem and base64.encodestring(
                    open(fname_cer_no_pem, "r").read()) or ''
                    
                fname_key_no_pem = file_globals['fname_key']
                keyCSD = fname_key_no_pem and base64.encodestring(
                    open(fname_key_no_pem, "r").read()) or ''
                    
                cfdi = base64.encodestring(xml_res_str_addenda)
                zip = False  # Validar si es un comprimido zip, con la extension del archivo
                contrasenaCSD = file_globals.get('password', '')
                
                if invoice.is_nomina:
                    
                    resultado = wsdl_client.service.stamp(cfdi, user, password)
                    
                    if resultado["Incidencias"] is None:                            
                        
                        htz = int(invoice_obj._get_time_zone(cr, uid, [ir_attachment_facturae_mx_id.invoice_id.id], context=context))
                        mensaje = resultado["CodEstatus"]
                        resultados_mensaje = ""
                        
                        folio_fiscal = resultado["UUID"]
                        fecha_timbrado = resultado['Fecha'] or False
                        fecha_timbrado = fecha_timbrado and time.strftime(
                            '%Y-%m-%d %H:%M:%S', time.strptime(
                                fecha_timbrado[:19], '%Y-%m-%dT%H:%M:%S')) or False
                        
                        fecha_timbrado = fecha_timbrado and datetime.strptime(
                            fecha_timbrado, '%Y-%m-%d %H:%M:%S') + timedelta(
                                hours=htz) or False
                        
                        rfc_emisor = invoice.company_emitter_id.partner_id.rfc
                        rfc_receptor = invoice.partner_id.rfc
                        
                        if invoice.is_nomina:                        
                            total_factura = str(invoice.total_nomina)
                        else:
                            total_factura = str(invoice.amount_total)
                        
#                         _logger.debug("?re=" + rfc_emisor + "&rr=" + rfc_receptor + "&tt=" + total_factura + "&id=" + folio_fiscal)
#                         
#                         img = qrcode.make("?re=" + rfc_emisor + "&rr=" + rfc_receptor + "&tt=" + total_factura + "&id=" + folio_fiscal)
#                         f = open("/tmp/io_cbb.png", "wb")
#                         _logger.debug(f)
#                         
#                         img.save(f)                        
#                         f.close()
                        
                        cfdi_data = {
                            #'cfdi_cbb': resultado['resultados']['qrCode'] or False,  # ya lo regresa en base64
                            'cfdi_sello': resultado['SatSeal'] or False,
                            'cfdi_no_certificado': resultado['NoCertificadoSAT'] or False,
                            'cfdi_cadena_original': cfd_data or False,
                            'cfdi_fecha_timbrado': fecha_timbrado,
                            'cfdi_xml': resultado["xml"],
                            'cfdi_folio_fiscal': folio_fiscal or '',
                            'pac_id': pac_params.id,
                        }
                        
#                         _logger.debug(cfdi_data)
#                         
#                         with open ("/tmp/io_cbb.png", "rb") as myfile:
#                             data=myfile.read()
#                             cfdi_data["cfdi_cbb"] = base64.b64encode(data)
                        
                        msg += mensaje + "." + resultados_mensaje + \
                            " Folio Fiscal: " + folio_fiscal + "."
                        msg += _(
                                u"\nMake Sure to the file really has generated correctly to the SAT\nhttps://www.consulta.sat.gob.mx/sicofi_web/moduloECFD_plus/ValidadorCFDI/Validador%20cfdi.html")
                        if cfdi_data.get('cfdi_xml', False):
                            url_pac = '</"%s"><!--Para validar el XML CFDI puede descargar el certificado del PAC desde la siguiente liga: https://solucionfactible.com/cfdi/00001000000102699425.zip-->' % (
                                comprobante)
                            cfdi_data['cfdi_xml'] = cfdi_data[
                                'cfdi_xml'].replace('</"%s">' % (comprobante), url_pac)
                            file = base64.encodestring(
                                cfdi_data['cfdi_xml'] or '')
#                                 invoice_obj.cfdi_data_write(cr, uid, [invoice.id],
#                                 cfdi_data, context=context)
                            cfdi_xml = cfdi_data.pop('cfdi_xml')
                        if cfdi_xml:
                            invoice_obj.write(cr, uid, [invoice.id], cfdi_data)
                            cfdi_data['cfdi_xml'] = cfdi_xml
                        else:
                            msg += _(u"Can't extract the file XML of PAC")
                    else:
                        
#                         cfdi_data = {
#                             #'cfdi_cbb': resultado['resultados']['qrCode'] or False,  # ya lo regresa en base64
#                             'cfdi_sello': False,
#                             'cfdi_no_certificado':  False,
#                             'cfdi_cadena_original': base64.decodestring(cfdi),
#                             'cfdi_fecha_timbrado': False,
#                             'cfdi_xml': base64.decodestring(cfdi),
#                             'cfdi_folio_fiscal': '',
#                             'pac_id': pac_params.id,
#                         }
#                         
#                         file = base64.decodestring(cfdi)
#                         cfdi_xml = cfdi_data.pop('cfdi_xml')
                        incidencia = resultado["Incidencias"]["Incidencia"][0]
                        raise osv.except_osv(_('Warning'), 
                            "Codigo de Error " + incidencia["CodigoError"] + " : " + incidencia["MensajeIncidencia"] + ", ")                       
                        
                else:
                    params = [user, password, cfdi, zip]
                    wsdl_client.soapproxy.config.dumpSOAPOut = 0
                    wsdl_client.soapproxy.config.dumpSOAPIn = 0
                    wsdl_client.soapproxy.config.debug = 0
                    wsdl_client.soapproxy.config.dict_encoding = 'UTF-8'
                    resultado = wsdl_client.timbrar(*params)
                
                    htz = int(invoice_obj._get_time_zone(
                        cr, uid, [ir_attachment_facturae_mx_id.invoice_id.id], context=context))
                    mensaje = _(tools.ustr(resultado['mensaje']))
                    resultados_mensaje = resultado['resultados'] and \
                        resultado['resultados']['mensaje'] or ''
                    folio_fiscal = resultado['resultados'] and \
                        resultado['resultados']['uuid'] or ''
                    codigo_timbrado = resultado['status'] or ''
                    codigo_validacion = resultado['resultados'] and \
                        resultado['resultados']['status'] or ''
                    if codigo_timbrado == '311' or codigo_validacion == '311':
                        raise osv.except_osv(_('Warning'), _(
                            'Unauthorized.\nCode 311'))
                    elif codigo_timbrado == '312' or codigo_validacion == '312':
                        raise osv.except_osv(_('Warning'), _(
                            'Failed to consult the SAT.\nCode 312'))
                    elif codigo_timbrado == '200' and codigo_validacion == '200':
                        fecha_timbrado = resultado[
                            'resultados']['fechaTimbrado'] or False
                        fecha_timbrado = fecha_timbrado and time.strftime(
                            '%Y-%m-%d %H:%M:%S', time.strptime(
                                fecha_timbrado[:19], '%Y-%m-%dT%H:%M:%S')) or False
                        fecha_timbrado = fecha_timbrado and datetime.strptime(
                            fecha_timbrado, '%Y-%m-%d %H:%M:%S') + timedelta(
                                hours=htz) or False
                        cfdi_data = {
                            'cfdi_cbb': resultado['resultados']['qrCode'] or False,  # ya lo regresa en base64
                            'cfdi_sello': resultado['resultados'][
                            'selloSAT'] or False,
                            'cfdi_no_certificado': resultado['resultados'][
                            'certificadoSAT'] or False,
                            'cfdi_cadena_original': resultado['resultados'][
                            'cadenaOriginal'] or False,
                            'cfdi_fecha_timbrado': fecha_timbrado,
                            'cfdi_xml': base64.decodestring(resultado[
                            'resultados']['cfdiTimbrado'] or ''),  # este se necesita en uno que no es base64
                            'cfdi_folio_fiscal': resultado['resultados']['uuid'] or '',
                            'pac_id': pac_params.id,
                        }
                        
                        msg += mensaje + "." + resultados_mensaje + \
                            " Folio Fiscal: " + folio_fiscal + "."
                        msg += _(
                                u"\nMake Sure to the file really has generated correctly to the SAT\nhttps://www.consulta.sat.gob.mx/sicofi_web/moduloECFD_plus/ValidadorCFDI/Validador%20cfdi.html")
                        if cfdi_data.get('cfdi_xml', False):
                            url_pac = '</"%s"><!--Para validar el XML CFDI puede descargar el certificado del PAC desde la siguiente liga: https://solucionfactible.com/cfdi/00001000000102699425.zip-->' % (
                                comprobante)
                            cfdi_data['cfdi_xml'] = cfdi_data[
                                'cfdi_xml'].replace('</"%s">' % (comprobante), url_pac)
                            file = base64.encodestring(
                                cfdi_data['cfdi_xml'] or '')
#                                 invoice_obj.cfdi_data_write(cr, uid, [invoice.id],
#                                 cfdi_data, context=context)
                            cfdi_xml = cfdi_data.pop('cfdi_xml')
                        if cfdi_xml:
                            invoice_obj.write(cr, uid, [invoice.id], cfdi_data)
                            cfdi_data['cfdi_xml'] = cfdi_xml
                        else:
                            msg += _(u"Can't extract the file XML of PAC")
                    else:
                        raise orm.except_orm(_('Warning'), _('Stamped Code: %s.-Validation code %s.-Folio Fiscal: %s.-Stamped Message: %s.-Validation Message: %s.') % (
                            codigo_timbrado, codigo_validacion, folio_fiscal, mensaje, resultados_mensaje))
            else:
                msg += 'Not found information from web services of PAC, verify that the configuration of PAC is correct'
                raise osv.except_osv(_('Warning'), _(
                    'Not found information from web services of PAC, verify that the configuration of PAC is correct'))
        return {'file': file, 'msg': msg, 'cfdi_xml': cfdi_xml}
                              
    def sf_cancel(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        msg = ''
        certificate_obj = self.pool.get('res.company.facturae.certificate')
        pac_params_obj = self.pool.get('params.pac')
        invoice_obj = self.pool.get('account.invoice')
        status_uuid = ""
        for ir_attachment_facturae_mx_id in self.browse(cr, uid, ids, context=context):
            status = False
            invoice = ir_attachment_facturae_mx_id.invoice_id
            
            if invoice.is_nomina:                
                pac_params_ids = pac_params_obj.search(cr, 1, [
                    ('method_type', '=', 'pac_fk_cancelar'), (                    
                            'active', '=', True)], limit=1, context=context)                                                       
            
            else:
                pac_params_ids = pac_params_obj.search(cr, 1, [
                    ('method_type', '=', 'pac_sf_cancelar'), (                    
                            'active', '=', True)], limit=1, context=context)
                
            pac_params_id = pac_params_ids and pac_params_ids[0] or False
            
            if pac_params_id:
            
                file_globals = invoice_obj._get_file_globals(
                    cr, uid, [invoice.id], context=context)
                pac_params_brw = pac_params_obj.browse(
                    cr, 1, [pac_params_id], context=context)[0]
                
                usuario = self.pool.get("res.users").browse(cr, 1, uid, context)    
                rfc = usuario.company_id.vat[2:]
                
                user = pac_params_brw.user
                password = pac_params_brw.password
                wsdl_url = pac_params_brw.url_webservice
                namespace = pac_params_brw.namespace
                
                if invoice.is_nomina:
                    wsdl_client = False
                    wsdl_client = Client(wsdl_url, cache=None)
                    
                    if wsdl_client:                               
                    
                        fname_cer_no_pem = file_globals['fname_cer']
                        cerCSD = fname_cer_no_pem and base64.encodestring(
                            open(fname_cer_no_pem, "r").read()) or ''
                        fname_key_no_pem = file_globals['fname_key']
                        keyCSD = fname_key_no_pem and base64.encodestring(
                            open(fname_key_no_pem, "r").read()) or ''
                        zip = False  # Validar si es un comprimido zip, con la extension del archivo
                        contrasenaCSD = file_globals.get('password', '')
                        invoices = [invoice.cfdi_folio_fiscal]  # cfdi_folio_fiscal
                        
                        print (wsdl_client)
                        
                        invoices_list = wsdl_client.factory.create("UUIDS")
                        invoices_list.uuids.string = invoices
                        
                        result = wsdl_client.service.cancel(invoices_list, user, password, rfc, cerCSD, keyCSD)
                        estatus = ""
                        folios = False
                        codestatus = False
                        
                        for name in result:
                            if name[0] == "Folios":                                
                                folios= True
                                estatus = result["Folios"]["Folio"][0]["EstatusUUID"]
                                break
                            elif name[0] == "CodEstatus":
                                codestatus = True
                                estatus = result["CodEstatus"]
                                break
                                
                        if folios and estatus == "201":
                            
                            rfcEmisor = result["RfcEmisor"]
                            folios = result["Folios"]
                            folio_fiscal = folios["Folio"]["UUID"]
                            status_uuid = folios["Folio"]["EstatusUUID"]                        
                            acuse = result["Acuse"]                       
                            
                            fecha_timbrado = result["Fecha"] or False                        
                            fecha_timbrado = fecha_timbrado and time.strftime(
                                '%Y-%m-%d %H:%M:%S', time.strptime(
                                    fecha_timbrado[:19], '%Y-%m-%dT%H:%M:%S')) or False
                            htz = int(invoice_obj._get_time_zone(cr, uid, [ir_attachment_facturae_mx_id.invoice_id.id], context=context))
                            fecha_timbrado = fecha_timbrado and datetime.strptime(
                                fecha_timbrado, '%Y-%m-%d %H:%M:%S') + timedelta(
                                    hours=htz) or False
                            msg = "El proceso de cancelación se ha completado correctamente"
                            invoice_obj.write(cr, uid, [invoice.id], {
                                'cfdi_fecha_cancelacion': time.strftime(
                                '%Y-%m-%d %H:%M:%S')
                            })
                            
                            status = True                       
                        else:
                            raise osv.except_osv("Error", "Error, el sistema devolvio el siguiente codigo " + estatus or "")
                            return {"error" : True, "description" : status}
                    else:
                        status = False
                        msg = "No se ha configurado la información del PAC"  
                        status_uuid = 0     
                else:
                    wsdl_client = False
                    wsdl_client = WSDL.SOAPProxy(wsdl_url, namespace)
                    fname_cer_no_pem = file_globals['fname_cer']
                    cerCSD = fname_cer_no_pem and base64.encodestring(
                        open(fname_cer_no_pem, "r").read()) or ''
                    fname_key_no_pem = file_globals['fname_key']
                    keyCSD = fname_key_no_pem and base64.encodestring(
                        open(fname_key_no_pem, "r").read()) or ''
                    zip = False  # Validar si es un comprimido zip, con la extension del archivo
                    contrasenaCSD = file_globals.get('password', '')
                    uuids = invoice.cfdi_folio_fiscal  # cfdi_folio_fiscal
                    params = [
                        user, password, uuids, cerCSD, keyCSD, contrasenaCSD]
                    wsdl_client.soapproxy.config.dumpSOAPOut = 0
                    wsdl_client.soapproxy.config.dumpSOAPIn = 0
                    wsdl_client.soapproxy.config.debug = 0
                    wsdl_client.soapproxy.config.dict_encoding = 'UTF-8'
                    result = wsdl_client.cancelar(*params)
                    codigo_cancel = result['status'] or ''
                    status_cancel = result['resultados'] and result[
                        'resultados']['status'] or ''
                    uuid_nvo = result['resultados'] and result[
                        'resultados']['uuid'] or ''
                    mensaje_cancel = _(tools.ustr(result['mensaje']))
                    msg_nvo = result['resultados'] and result[
                        'resultados']['mensaje'] or ''
                    status_uuid = result['resultados'] and result[
                        'resultados']['statusUUID'] or ''
                    folio_cancel = result['resultados'] and result[
                        'resultados']['uuid'] or ''
                    if codigo_cancel == '200' and status_cancel == '200' and\
                            status_uuid == '201':
                        msg +=  mensaje_cancel + _('\n- The process of cancellation\
                        has completed correctly.\n- The uuid cancelled is:\
                        ') + folio_cancel
                        invoice_obj.write(cr, uid, [invoice.id], {
                            'cfdi_fecha_cancelacion': time.strftime(
                            '%Y-%m-%d %H:%M:%S')
                        })
                        status = True
                    else:
                        raise orm.except_orm(_('Warning'), _('Cancel Code: %s.-Status code %s.-Status UUID: %s.-Folio Cancel: %s.-Cancel Message: %s.-Answer Message: %s.') % (
                            codigo_cancel, status_cancel, status_uuid, folio_cancel, mensaje_cancel, msg_nvo))            
            else:
                msg = _('Not found information of webservices of PAC, verify that the configuration of PAC is correct')                                     
        return {'message': msg, 'status_uuid': status_uuid, 'status': status}
                
                
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
