# -*- coding: utf-8 -*-
from openerp.osv import osv, fields

class hr_employee(osv.osv):
    _inherit = "hr.employee"
    
    _columns = {
                "rfc_employee" : fields.char("RFC"),
                "no_employee" : fields.char("No. Empleado"),
                "patronal" : fields.char("Registro Patronal"),
    }
    
hr_employee()

class hr_contract(osv.osv):
    _inherit = "hr.contract"
    
    def get_contract(self, cr, uid, employee, date_from, date_to, context=None):
        """
        @param employee: browse record of employee
        @param date_from: date field
        @param date_to: date field
        @return: returns the ids of all the contracts for the given employee that need to be considered for the given dates
        """
        contract_obj = self.pool.get('hr.contract')
        clause = []
        #a contract is valid if it ends between the given dates
        clause_1 = ['&',('date_end', '<=', date_to),('date_end','>=', date_from)]
        #OR if it starts between the given dates
        clause_2 = ['&',('date_start', '<=', date_to),('date_start','>=', date_from)]
        #OR if it starts before the date_from and finish after the date_end (or never finish)
        clause_3 = ['&',('date_start','<=', date_from),'|',('date_end', '=', False),('date_end','>=', date_to)]
        clause_final =  [('employee_id', '=', employee.id),'|','|'] + clause_1 + clause_2 + clause_3
        contract_ids = contract_obj.search(cr, uid, clause_final, context=context)
        return contract_ids
    
    _columns = {
            'integrated_wage': fields.float('Salario Integrado', digits=(12, 2), required=True, help="Utilizado para calculo de cuotas IMSS"),
#             'isr_table': fields.many2one('dg.isr.table.type', "Tabla de ISR", required=True, help="Utilizado para calculo de ISR"),
            'aguinaldo' : fields.integer('Dias de Aguinaldo'),
            'vacaciones' : fields.integer('Dias de Vaciones'),
            'prima_v' : fields.float('Prima Vacacional %', digits=(12,2)),
            "schedule_pay" : fields.selection([
                ('Diario', 'Diario'),
                ('Semanal', 'Semanal'),
                ('Quincenal', 'Quincenal'),
                ('Decenal', 'Decenal'),
                ('Catorcena Mensual', 'Catorcena Mensual'),
                ('Mensual', 'Mensual'),
                ('Bimestral', 'Bimestral'),
                ('Unidad de Obra', 'Unidad de Obra'),
                ('Comisión', 'Comisión'),
                ('Precio Alzado', 'Precio Alzado'),
                ], 'Periodo de Pago', select=True),
            "regimen_employee" : fields.selection([
                  (2, "2 - Sueldos y salarios"),
                  (3, "3 - Jubilados"),
                  (4, "4 - Pensionados"),
                  (5, "5 - Asimilados a salarios, Miembros de las Sociedades Cooperativas de Producción"),
                  (6, "6 - Asimilados a salarios, Integrantes de Sociedades y Asociaciones Civiles"),
                  (7, "7 - Asimilados a salarios, Miembros de consejos directivos, de vigilancia, consultivos, honorarios a administradores, comisarios y gerentes generales"),
                  (8, "8 - Asimilados a salarios, Actividad empresarial (comisionistas)"),
                  (9, "9 - Asimilados a salarios, Honorarios asimilados a salarios"),
                  (10, "10 - Asimilados a salarios, Ingresos acciones o títulos valor"),                                                          
                  ], "Regimen del Empleado"),   
    }
    
    _defaults = {  
        'schedule_pay': "Quincenal",  
        }
    
hr_contract()