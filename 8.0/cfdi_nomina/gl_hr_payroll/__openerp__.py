# -*- coding: utf-8 -*-
{
    'name': 'Ikom Custom RH',
    'version': '1.0.0',
    'category': 'RH',
    'sequence': 1,
    'author': 'Gerardo A Lopez Vega @glopzvega',
    'website': 'http://www.ikom.mx',
    'summary': 'Modulo de personalizaciones del modulo de RH',
    'description': '''Modulo de personalizaciones del modulo de RH, este modulo agrega la siguiente funcionalidad
                        Agrega los campos requeridos para empleados y contratos utilizados en mexico.
                        Adicionalmente se ocultan algunos campos que no son necesarios en el formulario del empleado.''',
    'depends': ["hr_payroll", "ikom_custom"],    
    'data': [
             "hr_view.xml",
             "custom.sql"        
    ],    
    'installable': True,
    'active': False
}