# -*- coding: utf-8 -*-
{
    'name' : 'io_base_vat',
    'version' : '1.0',
    'author' : 'iozoft.com @glopzvega',
    'category' : 'Accounting & Finance',
    'sequence': 0,
    'description' : """
    RFC Personalizado
=============================================

Financial and accounting module that covers:
--------------------------------------------
    * General Accounting
    * Cost/Analytic accounting
    * Third party accounting
    * Taxes management
    * Budgets
    * Customer and Supplier Invoices
    * Bank statements
    * Reconciliation process by partner

Creates a dashboard for accountants that includes:
--------------------------------------------------
    * List of Customer Invoices to Approve
    * Company Analysis
    * Graph of Treasury

Processes like maintaining general ledgers are done through the defined Financial Journals (entry move line or grouping is maintained through a journal) 
for a particular financial year and for preparation of vouchers there is a module named account_voucher.
    """,
    'website': 'http://www.iozoft.com',
    'depends' : ['base_vat'],
    'data': [
             'res_partner_view.xml'        
    ],
    'qweb' : [
    ],
    'demo': [
    ],
    'test': [
    ],
    'installable': True,
    'auto_install': False,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
