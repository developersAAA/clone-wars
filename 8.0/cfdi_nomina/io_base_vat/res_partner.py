# -*- encoding: utf-8 -*-

from openerp.osv import osv, fields

class res_partner(osv.osv):
    
    _inherit = "res.partner"
    
    _columns = {
        "rfc" : fields.char("RFC")
    }
    
    _defaults = {  
        'rfc': "XAXX010101000",
        'vat': "MXXAXX010101000"  
    }
    
#     def _split_vat(self, vat):
#         vat_country = "MX"
#         return vat_country, vat
    
    def rfc_change(self, cr, uid, ids, value, context=None):
        return {'value': {'vat': 'MX' + value}}
    
    def create(self, cr, uid, vals, context=None):
        if vals.has_key("rfc") and vals["rfc"]:    
            vals["vat"] = "MX" + vals["rfc"] or "XAXX010101000"
        else:
            vals["rfc"] = "XAXX010101000"
            vals["vat"] = "MX" + vals["rfc"]
            
        res = super(res_partner, self).create(cr, uid, vals, context)
        return res
    
    def write(self, cr, uid, ids, vals, context=None):
        
        if vals.has_key("rfc") and vals["rfc"]:    
            vals["vat"] = "MX" + vals["rfc"] or "XAXX010101000"
        
        res = super(res_partner, self).write(cr, uid, ids, vals, context)
        return res
    
res_partner()