# -*- coding: utf-8 -*-
{
    "name" : "Escato - Sale",
    "version" : "0.1",
    "description" : """

    """,
    "author" : "ikom",
    "website" : "http://www.ikom.mx",
    "depends" : [
        "sale",
        "mrp",
    ],
    "category" : "Custom Modules",
    "init_xml" : [],
    "demo_xml" : [],
    "update_xml" : [
        "sale_view.xml"
    ],
    "active": False,
    "installable": True
}
