# -*- coding: utf-8 -*-
from openerp.osv import fields,osv
import math

class product_product(osv.osv):
    _inherit = 'product.product'
    _columns = {
        'bom_id': fields.many2one('mrp.bom','BoM'),
        'fixed_price': fields.boolean( 'Fixed price', help='Mark this field if the public price of the  should be fixed' ),
    }
product_product()

class res_color(osv.osv):
    _name = 'res.color'
    _description = 'Color'
    _columns = {
        'name': fields.char('Color'),
    }

class sale_order_line(osv.osv):
    _inherit = 'sale.order.line'
    _columns = {
        'pack_depth': fields.integer('Depth', required=True, help='Depth of the product'),
        'pack_parent_line_id': fields.many2one('sale.order.line', 'Pack', help='The BoM that contains this product.'),
        'pack_child_line_ids': fields.one2many('sale.order.line', 'pack_parent_line_id', 'Lines in pack', help=''),
        'color': fields.many2one('res.color','Color'),
        'finish_model': fields.char('Finish Model'),
    }
    _defaults = {
        'pack_depth': lambda *a: 0,
    }
sale_order_line()

class sale_order(osv.osv):
    _inherit = 'sale.order'


    def search_mrp_bom(self, cr, uid, ids, product, context=None):
        if context is None: context = {}
        bom_obj = self.pool.get()

    def copy(self, cr, uid, id, default=None, context=None):
        if not default:
            default = {}
        for sale in self.browse(cr, uid, id, context=context):
            for line in sale.order_line:
                if line.pack_depth:
                    default.update({
                        'state': 'draft',
                        'shipped': False,
                        'order_line': [],
                        'invoice_ids': [],
                        'picking_ids': [],
                        'date_confirm': False,
                        'name': self.pool.get('ir.sequence').get(cr, uid, 'sale.order'),
                    })
                else:
                    pass
        return super(sale_order, self).copy(cr, uid, id, default, context=context)

    def create(self, cr, uid, vals, context=None):
        result = super(sale_order,self).create(cr, uid, vals, context)
        self.expand_components(cr, uid, [result], context)
        return result

    def write(self, cr, uid, ids, vals, context=None):
        result = super(sale_order,self).write(cr, uid, ids, vals, context)
        self.expand_components(cr, uid, ids, context)
        return result

    def expand_components(self, cr, uid, ids, context={}, depth=1):
        if depth == 10:
            return
        updated_orders = []
        for order in self.browse(cr, uid, ids, context):

            fiscal_position = order.fiscal_position and self.pool.get('account.fiscal.position').browse(cr, uid, order.fiscal_position, context) or False

            sequence = -1
            reorder = []
            last_had_children = False
            for line in order.order_line:
                if last_had_children and not line.pack_parent_line_id:
                    reorder.append( line.id )
                    if line.product_id.bom_id.bom_line_ids and not order.id in updated_orders:
                        updated_orders.append( order.id )
                    continue

                sequence += 1

                if sequence > line.sequence:
                    self.pool.get('sale.order.line').write(cr, uid, [line.id], {
                        'sequence': sequence,
                    }, context)
                else:
                    sequence = line.sequence

                if line.state != 'draft':
                    continue
                if not line.product_id:
                    continue

                # If pack was already expanded (in another create/write operation or in
                # a previous iteration) don't do it again.
                if line.pack_child_line_ids:
                    last_had_children = True
                    continue
                last_had_children = False

                for subline in line.product_id.bom_id.bom_line_ids:
                    sequence += 1

                    subproduct = subline.product_id
                    quantity = subline.product_qty * line.product_uom_qty

                    if line.product_id.fixed_price:
                        price = 0.0
                        discount = 0.0
                    else:
                        pricelist = order.pricelist_id.id
                        price = self.pool.get('product.pricelist').price_get(cr, uid, [pricelist],
                                        subproduct.id, quantity, order.partner_id.id, {
                            'uom': subproduct.uom_id.id,
                            'date': order.date_order,
                        })[pricelist]
                        discount = line.discount

                    # Obtain product name in partner's language
                    ctx = {'lang': order.partner_id.lang}
                    subproduct_name = self.pool.get('product.product').browse(cr, uid, subproduct.id, ctx).name

                    tax_ids = self.pool.get('account.fiscal.position').map_tax(cr, uid, fiscal_position, subproduct.taxes_id)

                    if subproduct.uos_id:
                        uos_id = subproduct.uos_id.id
                        uos_qty = quantity * subproduct.uos_coeff
                    else:
                        uos_id = False
                        uos_qty = quantity

                    vals = {
                        'order_id': order.id,
                        'name': '%s%s' % ('> '* (line.pack_depth+1), subproduct_name),
                        'sequence': sequence,
                        'delay': subproduct.sale_delay or 0.0,
                        'product_id': subproduct.id,
                        'price_unit': price,
                        'tax_id': [(6,0,tax_ids)],
                        'property_ids': [(6,0,[])],
                        'address_allotment_id': False,
                        'product_uom_qty': quantity,
                        'product_uom': subproduct.uom_id.id,
                        'product_uos_qty': uos_qty,
                        'product_uos': uos_id,
                        'product_packaging': False,
                        'move_ids': [(6,0,[])],
                        'discount': discount,
                        'number_packages': False,
                        'notes': False,
                        'th_weight': False,
                        'state': 'draft',
                        'pack_parent_line_id': line.id,
                        'pack_depth': line.pack_depth + 1,
                    }

                    # It's a control for the case that the nan_external_prices was installed with the product pack
                    if 'prices_used' in line:
                        vals[ 'prices_used' ] = line.prices_used

                    self.pool.get('sale.order.line').create(cr, uid, vals, context)
                    if not order.id in updated_orders:
                        updated_orders.append( order.id )

                for id in reorder:
                    sequence += 1
                    self.pool.get('sale.order.line').write(cr, uid, [id], {
                        'sequence': sequence,
                    }, context)

        if updated_orders:
            # Try to expand again all those orders that had a pack in this iteration.
            # This way we support packs inside other packs.
            self.expand_components(cr, uid, ids, context, depth+1)
        return

    def action_ship_create(self, cr, uid, ids, context=None):
        if context is None: context = {}
        res = super(sale_order, self).action_ship_create(cr, uid, ids, context=context)
        move_list = []
        sale = self.browse(cr, uid, ids, context=context)[0]
        picking_obj = self.pool.get("stock.picking")
        picking_ids = picking_obj.search(cr, uid, [("origin","=",sale.name)])
        if len(picking_ids) >= 1:
            for picking in picking_obj.browse(cr, uid, picking_ids, context=context):
                for move in picking.move_lines:
                    if move.product_id.bom_id:
                        move.write({"state":"draft"})
                        move_list.append(move.id)
                picking.write({"move_lines": [(2,x) for x in move_list]})
                        
        return res


sale_order()