# -*- encoding: utf-8 -*-
{
    "name": "IKOM - MRP Repair",
    "version": "1.0",
    "author": "IKOM",
    "category": "",
    "website": "http://www.ikom.com.mx",
    "license": "",
    "depends": [
        "base",
        "mrp",
        "mrp_repair",
    ],
    "demo": [],
    "init_xml": [
        "security/groups.xml",
    ],
    "data": [
        "view/menu.xml"
    ],
    "test": [],
    "installable": True,
    "auto_install": False,
    "active": False
}
