function openerp_rmambo_models(instance,module){
    var QWeb = instance.web.qweb;
    var _t = instance.web._t;
    
    var _super_posmodel = module.PosModel.prototype;
    module.PosModel = module.PosModel.extend({
        initialize: function(session, attributes) {
            this.sections = [];
            return _super_posmodel.initialize.call(this,session,attributes);
        },
    });
    
    module.PosModel.prototype.models.push({
        model:  'restaurant.section',
        fields: ['id','name','complete_name','parent_id','child_id','image'],
        //domain: [['active','=',true]],
        loaded: function(self,sections){
            self.sections = sections;
            self.db.add_sections(sections);
        },
    });
    
    module.PosModel.prototype.models.push({
        model:  'restaurant.tservice',
        fields: ['id','name','complete_name','section_id'],
        //domain: [['active','=',true]],
        loaded: function(self,tables){
            self.tables = tables;
            self.db.add_tables(tables);
        },
    });
    
    var _super_orderline = module.Orderline.prototype;
    module.Orderline = module.Orderline.extend({
        initialize: function(attr,options) {
            this.note = options.note || "";
            return _super_orderline.initialize.call(this,attr,options);
        },
        set_note: function(note){
            this.note = note;
            this.trigger('change',this);
        },
        get_note: function(){
            return this.note;
        },
        export_as_JSON: function(){
            var json = _super_orderline.export_as_JSON.call(this);
            json.note = this.get_note();
            return json;
        },
    });
    
    var _super_order = module.Order.prototype;
    module.Order = module.Order.extend({
        initialize: function(attributes) {
            this.set({
                table: null,
            });
            return _super_order.initialize.call(this,attributes);
        },
       
        // the table related to the current order.
        set_table: function(table){
            this.set('table',table);
        },
        get_table: function(){
            return this.get('table');
        },
        get_table_name: function(){
            var table = this.get('table');
            return table ? table.complete_name : "";
        },
        export_as_JSON: function(){
            var json = _super_order.export_as_JSON.call(this);
            json.table_id = this.get_table() ? this.get_table().id : false;
            return json;
        },
    });
}
