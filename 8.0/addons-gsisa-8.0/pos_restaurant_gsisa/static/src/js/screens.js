function openerp_rmambo_screens(instance,module){
    var QWeb = instance.web.qweb;
    var _t = instance.web._t;
    
    //INICIO Intento agregar widget mesa
    module.TableScreenWidget = module.ScreenWidget.extend({
        template:'TableScreenWidget',

        show_numpad:     true,
        show_leftpane:   true,

        start: function(){ //FIXME this should work as renderElement... but then the categories aren't properly set. explore why
            var self = this;

            this.table_list_widget = new module.TableListWidget(this,{
                click_table_action: function(table){
                    self.pos.get('selectedOrder').set_table(table);
                    self.pos_widget.screen_selector.set_current_screen('products',{});
                },
                table_list: this.pos.db.get_table_by_section(0)
            });
            this.table_list_widget.replace(this.$('.placeholder-TableListWidget'));

            this.table_sections_widget = new module.TableSectionsWidget(this,{
                table_list_widget: this.table_list_widget,
            });
            this.table_sections_widget.replace(this.$('.placeholder-TableSectionsWidget'));
        },

        show: function(){
            this._super();
            var self = this;

            this.table_sections_widget.reset_section();
            this.pos_widget.order_widget.set_editable(true);
        },

        close: function(){
            this._super();

            this.pos_widget.order_widget.set_editable(false);

            if(this.pos.config.iface_vkeyboard && this.pos_widget.onscreen_keyboard){
                this.pos_widget.onscreen_keyboard.hide();
            }
        },
    });
    //FIN Intento agregar widget mesa
    
    module.TextInputPopupWidget = module.PopUpWidget.extend({
        template: 'TextInputPopupWidget',
        show: function(options){
            var self = this;
            this._super();

            this.message = options.message || '';
            this.comment = options.comment || '';
            this.renderElement();
            if(options.value){
                this.$('input,textarea').val(options.value);
            }
            this.$('.button.cancel').click(function(){
                self.pos_widget.screen_selector.close_popup();
                if( options.cancel ){
                    options.cancel.call(self);
                }
            });

            this.$('.button.confirm').click(function(){
                var value = self.$('input,textarea').val();
                self.pos_widget.screen_selector.close_popup();
                if( options.confirm ){
                    options.confirm.call(self, value);
                }
            });
        },
    });
    
    module.PosWidget.include({
        build_widgets: function(){
            var self = this;
            this._super();
            
            //INICIO Intento agregar widget table
            this.table_screen = new module.TableScreenWidget(this,{});
            this.table_screen.appendTo(this.$('.screens'));
            //FIN Intento agregar widget table
            this.screen_selector.add_screen('tables',this.table_screen);
            
            this.text_input_popup = new module.TextInputPopupWidget(this,{});
            this.text_input_popup.appendTo(this.$el);            
            this.text_input_popup.hide();
            this.screen_selector.popup_set['text-input'] = this.text_input_popup;
        }
    });
}
