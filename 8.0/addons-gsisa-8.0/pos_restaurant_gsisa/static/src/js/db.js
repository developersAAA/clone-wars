function openerp_rmambo_db(instance,module){
    var QWeb = instance.web.qweb;
    var _t = instance.web._t;
    
    module.PosDB.include({
        init: function(options){
            this._super(options);  // calling the original init() method
            this.section_by_id = {};
            this.root_section_id  = 0;
            this.section_tables = {};
            this.section_ancestors = {};
            this.section_childs = {};
            this.section_parent    = {};
            this.section_search_string = {};
            
            this.table_by_id = {};
            this.table_by_section_id = {};
        },
        get_section_by_id: function(section_id){
            if(section_id instanceof Array){
                var list = [];
                for(var i = 0, len = section_id.length; i < len; i++){
                    var section = this.section_by_id[section_id[i]];
                    if(section){
                        list.push(section);
                    }else{
                        console.error("get_section_by_id: no section has id:",section_id[i]);
                    }
                }
                return list;
            }else{
                return this.section_by_id[section_id];
            }
        },
        
        /* returns a list of the category's child categories ids, or an empty list 
         * if a category has no childs */
        get_section_childs_ids: function(section_id){
            return this.section_childs[section_id] || [];
        },
        /* returns a list of all ancestors (parent, grand-parent, etc) categories ids
         * starting from the root category to the direct parent */
        get_section_ancestors_ids: function(section_id){
            return this.section_ancestors[section_id] || [];
        },
        /* returns the parent category's id of a category, or the root_category_id if no parent.
         * the root category is parent of itself. */
        get_section_parent_id: function(section_id){
            return this.section_parent[section_id] || this.root_section_id;
        },
        
        add_sections: function(sections){
            var self = this;
            if(!this.section_by_id[this.root_section_id]){
                this.section_by_id[this.root_section_id] = {
                    id : this.root_section_id,
                    name : 'Root',
                };
            }
            
            for(var i=0, len = sections.length; i < len; i++){
                this.section_by_id[sections[i].id] = sections[i];
            }
            for(var i=0, len = sections.length; i < len; i++){
                var sec = sections[i];
                var parent_id = sec.parent_id[0] || this.root_section_id;
                this.section_parent[sec.id] = sec.parent_id[0];
                if(!this.section_childs[parent_id]){
                    this.section_childs[parent_id] = [];
                }
                this.section_childs[parent_id].push(sec.id);
            }
            
            function make_ancestors(sec_id, ancestors){
                self.section_ancestors[sec_id] = ancestors;

                ancestors = ancestors.slice(0);
                ancestors.push(sec_id);

                var childs = self.section_childs[sec_id] || [];
                for(var i=0, len = childs.length; i < len; i++){
                    make_ancestors(childs[i], ancestors);
                }
            }
            make_ancestors(this.root_section_id, []);
        },
        
        add_tables: function(tables){
            var stored_tables = this.table_by_section_id;

            if(!tables instanceof Array){
                tables = [tables];
            }
            for(var i = 0, len = tables.length; i < len; i++){
                var table = tables[i];
                //var search_string = this._table_search_string(table);
                var section_id = table.section_id ? table.section_id[0] : this.root_section_id;
                //table.product_tmpl_id = table.product_tmpl_id[0];
                if(!stored_tables[section_id]){
                    stored_tables[section_id] = [];
                }
                stored_tables[section_id].push(table.id);

                var ancestors = this.get_section_ancestors_ids(section_id) || [];

                for(var j = 0, jlen = ancestors.length; j < jlen; j++){
                    var ancestor = ancestors[j];
                    if(! stored_tables[ancestor]){
                        stored_tables[ancestor] = [];
                    }
                    stored_tables[ancestor].push(table.id);
                }
                this.table_by_id[table.id] = table;
            }
        },
        
        get_table_by_section: function(section_id){
            var table_ids  = this.table_by_section_id[section_id];
            var list = [];
            if (table_ids) {
                for (var i = 0, len = Math.min(table_ids.length, this.limit); i < len; i++) {
                    list.push(this.table_by_id[table_ids[i]]);
                }
            }
            return list;
        },
        
        get_table_by_id: function(id){
            return this.table_by_id[id];
        },
        get_sections_sorted: function(max_count){
            max_count = max_count ? Math.min(this.section_sorted.length, max_count) : this.section_sorted.length;
            var sections = [];
            for (var i = 0; i < max_count; i++) {
                sections.push(this.section_by_id[this.section_sorted[i]]);
            }
            return sections;
        },
    });
}
