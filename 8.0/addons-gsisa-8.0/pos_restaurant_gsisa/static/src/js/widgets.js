function openerp_rmambo_widgets(instance,module){
    var QWeb = instance.web.qweb;
    var _t = instance.web._t;
    
    //INICIO
    module.NoteButtonWidget = module.PosBaseWidget.extend({
        template: 'NoteButtonWidget',
        renderElement: function() {
            var self = this;
            this._super();

            this.$el.click(function(){
                var line = self.pos.get_order().getSelectedLine();
                if (line) {
                    self.pos.pos_widget.screen_selector.show_popup('text-input',{
                        message: _t('Nota'),
                        value: line.get_note(),
                        confirm: function(note){
                                line.set_note(note);
                            },
                    });
                }
            });
        },
    });
    
    module.SendOrderButtonWidget = module.PosBaseWidget.extend({
        template: 'SendOrderButtonWidget',
        init: function(parent, options){
            this._super(parent, options);
            this.cashregister = options.cashregister;
        },
        renderElement: function() {
            var self = this;
            this._super();

            this.$el.click(function(){
                if (self.pos.get('selectedOrder').get('screen') === 'receipt'){  //TODO Why ?
                    console.warn('TODO should not get there...?');
                    return;
                }
                var currentOrder = self.pos.get('selectedOrder');
                if(currentOrder.get('orderLines').models.length === 0){
                    self.pos_widget.screen_selector.show_popup('error',{
                        'message': _t('Empty Order'),
                        'comment': _t('There must be at least one product in your order before it can be validated'),
                        });
                    return;
                }
                
                if(!currentOrder.get_table()){
                    self.pos_widget.screen_selector.show_popup('error',{
                        'message': _t('Mesa'),
                        'comment': _t('Debe de asociar la orden a una Mesa'),
                        });
                    return;
                }
                
                self.pos.push_order(currentOrder);
                self.pos.get('selectedOrder').destroy();    //finish order and go back to scan screen
                //self.pos.add_new_order();
                //self.pos_widget.screen_selector.set_current_screen(self.next_screen);
            });
        },
    });
    
    var _super_poswidget = module.PosBaseWidget.prototype;
    module.PaypadWidget.include({
        renderElement: function() {
            var self = this;
            _super_poswidget.renderElement.call(this);
            
            var button = new module.NoteButtonWidget(self,{
                pos: self.pos,
                pos_widget : self.pos_widget,
            });
            button.appendTo(self.$el);
            
            var button = new module.SendOrderButtonWidget(self,{
                pos: self.pos,
                pos_widget : self.pos_widget,
                cashregister: this.pos.cashregisters[0],
            });
            button.appendTo(self.$el);
        }
    });
    
    module.OrderButtonWidget.include({
        renderElement:function(){
            this.selected = ( this.pos.get('selectedOrder') === this.order )
            _super_poswidget.renderElement.call(this);
            var self = this;
            this.$el.click(function(){ 
                if( self.pos.get('selectedOrder') === self.order ){
                    var ss = self.pos.pos_widget.screen_selector;
                    if(ss.get_current_screen() === 'tables'){
                        ss.back();
                    }else if (ss.get_current_screen() !== 'receipt'){
                        ss.set_current_screen('tables');
                    }
                }else{
                    self.selectOrder();
                }
            });
            if( this.selected){
                this.$el.addClass('selected');
            }
        },
    });
    
    module.TableSectionsWidget = module.PosBaseWidget.extend({
        template: 'TableSectionsWidget',
        init: function(parent, options){
            var self = this;
            this._super(parent,options);
            this.section = this.pos.root_section;
            this.breadcrumb = [];
            this.subsections = [];
            this.table_list_widget = options.table_list_widget || null;
            this.section_cache = new module.DomCache();
            this.set_section();
            
            this.switch_section_handler = function(event){
                self.set_section(self.pos.db.get_section_by_id(Number(this.dataset['sectionId'])));
                self.renderElement();
            };
        },

        // changes the section. if undefined, sets to root section
        set_section : function(section){
            var db = this.pos.db;
            if(!section){
                this.section = db.get_section_by_id(db.root_section_id);
            }else{
                this.section = section;
            }
            this.breadcrumb = [];
            var ancestors_ids = db.get_section_ancestors_ids(this.section.id);
            for(var i = 1; i < ancestors_ids.length; i++){
                this.breadcrumb.push(db.get_section_by_id(ancestors_ids[i]));
            }
            if(this.section.id !== db.root_section_id){
                this.breadcrumb.push(this.section);
            }
            this.subsections = db.get_section_by_id(db.get_section_childs_ids(this.section.id));
            console.log("subsections:",this.subsections);
        },

        get_image_url: function(section){
            return window.location.origin + '/web/binary/image?model=restaurant.section&field=image_medium&id='+section.id;
        },

        render_section: function( section, with_image ){
            var cached = false; //this.section_cache.get_node(section.id);
            if(!cached){
                if(with_image){
                    var image_url = this.get_image_url(section);
                    var section_html = QWeb.render('SectionButton',{ 
                            widget:  this, 
                            section: section, 
                            image_url: this.get_image_url(section),
                        });
                        section_html = _.str.trim(section_html);
                    var section_node = document.createElement('div');
                        section_node.innerHTML = section_html;
                        section_node = section_node.childNodes[0];
                }else{
                    var section_html = QWeb.render('SectionSimpleButton',{ 
                            widget:  this, 
                            section: section, 
                        });
                        section_html = _.str.trim(section_html);
                    var section_node = document.createElement('div');
                        section_node.innerHTML = section_html;
                        section_node = section_node.childNodes[0];
                }
                this.section_cache.cache_node(section.id,section_node);
                return section_node;
            }
            return cached; 
        },
        
        replace: function($target){
            this.renderElement();
            var target = $target[0];
            target.parentNode.replaceChild(this.el,target);
        },
        
        renderElement: function(){
            var self = this;

            var el_str  = openerp.qweb.render(this.template, {widget: this});
            var el_node = document.createElement('div');
                el_node.innerHTML = el_str;
                el_node = el_node.childNodes[1];

            if(this.el && this.el.parentNode){
                this.el.parentNode.replaceChild(el_node,this.el);
            }

            this.el = el_node;

            var hasimages = true;  //if none of the subcategories have images, we don't display buttons with icons
            for(var i = 0; i < this.subsections.length; i++){
                if(this.subsections[i].image){
                    hasimages = true;
                    break;
                }
            }

            var list_container = el_node.querySelector('.section-list');
            if (list_container) { 
                if (!hasimages) {
                    list_container.classList.add('simple');
                } else {
                    list_container.classList.remove('simple');
                }
                for(var i = 0, len = this.subsections.length; i < len; i++){
                    list_container.appendChild(this.render_section(this.subsections[i],hasimages));
                };
            }

            var buttons = el_node.querySelectorAll('.js-section-switch');
            for(var i = 0; i < buttons.length; i++){
                buttons[i].addEventListener('click',this.switch_section_handler);
            }
            
            var tables = this.pos.db.get_table_by_section(this.section.id);
            this.table_list_widget.set_table_list(tables);
            
        },
        
        // resets the current category to the root category
        reset_section: function(){
            this.set_section();
            this.renderElement();
        },
    });
    
    module.TableListWidget = module.PosBaseWidget.extend({
        template:'TableListWidget',
        init: function(parent, options) {
            var self = this;
            this._super(parent,options);
            this.model = options.model;
            this.tablewidgets = [];
            this.next_screen = options.next_screen || false;

            this.click_table_handler = function(event){
                var table = self.pos.db.get_table_by_id(this.dataset['tableId']);
                options.click_table_action(table);
            };

            this.table_list = options.table_list || [];
            this.table_cache = new module.DomCache();
        },
        set_table_list: function(table_list){
            this.table_list = table_list;
            this.renderElement();
        },
        get_table_image_url: function(table){
            return window.location.origin + '/web/binary/image?model=product.product&field=image_medium&id=1';
        },
        replace: function($target){
            this.renderElement();
            var target = $target[0];
            target.parentNode.replaceChild(this.el,target);
        },

        render_table: function(table){
            var cached = this.table_cache.get_node(table.id);
            if(!cached){
                var image_url = this.get_table_image_url(table);
                var table_html = QWeb.render('Table',{ 
                        widget:  this, 
                        table: table, 
                        image_url: this.get_table_image_url(table),
                    });
                var table_node = document.createElement('div');
                table_node.innerHTML = table_html;
                table_node = table_node.childNodes[1];
                this.table_cache.cache_node(table.id,table_node);
                return table_node;
            }
            return cached;
        },

        renderElement: function() {
            var self = this;

            // this._super()
            var el_str  = openerp.qweb.render(this.template, {widget: this});
            var el_node = document.createElement('div');
                el_node.innerHTML = el_str;
                el_node = el_node.childNodes[1];

            if(this.el && this.el.parentNode){
                this.el.parentNode.replaceChild(el_node,this.el);
            }
            this.el = el_node;

            var list_container = el_node.querySelector('.table-list');
            for(var i = 0, len = this.table_list.length; i < len; i++){
                var table_node = this.render_table(this.table_list[i]);
                table_node.addEventListener('click',this.click_table_handler);
                list_container.appendChild(table_node);
            };
        },
    });
    
    //FINAL
}
