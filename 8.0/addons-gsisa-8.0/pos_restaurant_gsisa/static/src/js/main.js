openerp.pos_restaurant_gsisa = function(instance){
    var module = instance.point_of_sale;
    
    openerp_rmambo_db(instance,module);
    openerp_rmambo_models(instance,module);
    openerp_rmambo_widgets(instance,module);
    openerp_rmambo_screens(instance,module);
};
