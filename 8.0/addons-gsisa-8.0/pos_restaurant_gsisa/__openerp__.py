# -*- coding: utf-8 -*-
##############################################################################
#
#    Jose Antonio Silva Pablo (gsisa.asilva)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


{
    'name': 'Adaptacion para Restaurant&Cafe',
    'version': '1.1',
    'category': 'Generic Modules/',
    'description': """
    Modulo para Restaurant&Cafe
    """,
    'author': 'gsisa.asilva',
    'depends': ['point_of_sale'],
    'data': [
        'data/restaurant_data.xml',
        'restaurant_view.xml',
        'point_of_sale_view.xml',
        'views/templates.xml',
    ],
    'qweb':[
        'static/src/xml/pos.xml',
    ],
    'installable': True,
    'active': False,
    }
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
