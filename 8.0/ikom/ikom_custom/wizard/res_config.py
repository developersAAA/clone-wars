# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Business Applications
#    Copyright (C) 2004-2012 OpenERP S.A. (<http://openerp.com>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import fields, osv

class ikom_config_settings(osv.osv_memory):
    _name = 'ikom.config.settings'
    _inherit = 'res.config.settings'

    _columns = {
        'module_ikom_custom_sale': fields.boolean('Instala el modulo Ikom Custom de Ventas',
            help ="""Esto instala el modulo ikom_custom_sale."""),
        'module_ikom_custom_purchase': fields.boolean('Instala el modulo Ikom Custom de Compras',
            help ="""Esto instala el modulo ikom_custom_purchase."""),
        'module_ikom_custom_account': fields.boolean('Instala el modulo Ikom Custom de Contabilidad',
            help ="""Esto instala el modulo ikom_custom_account."""),
        
    }

    def execute(self, cr, uid, ids, context=None):

        module_obj = self.pool.get("ir.module.module")
        this = self.browse(cr, uid, ids[0], context=context)

        sale_module = this.module_ikom_custom_sale
        purchase_module = this.module_ikom_custom_purchase
        account_module = this.module_ikom_custom_account

        if sale_module:
            id_mod = module_obj.search(cr, uid, [("name", "=", "sale")], context=context)
            mod = module_obj.browse(cr, uid, id_mod[0], context=context)

            if mod.state != "installed":
                raise osv.except_osv('Error!',"Necesitas instalar el modulo de Ventas primero")

        if purchase_module:
            id_mod = module_obj.search(cr, uid, [("name", "=", "purchase")], context=context)
            mod = module_obj.browse(cr, uid, id_mod[0], context=context)

            if mod.state != "installed":
                raise osv.except_osv('Error!', "Necesitas instalar el modulo de Compras primero")

        if account_module:
            id_mod = module_obj.search(cr, uid, [("name", "=", "account")], context=context)
            mod = module_obj.browse(cr, uid, id_mod[0], context=context)

            if mod.state != "installed":
                raise osv.except_osv('Error!', "Necesitas instalar el modulo de Contabilidad primero")



        res = super(ikom_config_settings, self).execute(cr, uid, ids, context=context)
        return res


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4: