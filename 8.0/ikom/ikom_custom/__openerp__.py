# -*- coding: utf-8 -*-
{
    'name': 'Ikom Custom',
    'version': '1.0.0',
    'category': 'RH',
    'sequence': 1,
    'author': 'Gerardo A Lopez Vega @glopzvega',
    'website': 'http://www.desiteg.com',
    'summary': 'Modulo de personalizaciones Ikom',
    'description': '''Modulo de personalizaciones Ikom, Este modulo oculta algunos campos que no son necesarios en el formulario del cliente, 
                        ademas realiza las configuraciones a las monedas para que la separacion decimal sea correcta y aparezca el signo al 
                        principio de la cantidad''',
    'depends': ["mail"],    
    'data': [
             "ikom_custom.sql",
             "res_partner_view.xml",
             "wizard/res_config_view.xml"           
    ],    
    'installable': True,
    'active': False
}