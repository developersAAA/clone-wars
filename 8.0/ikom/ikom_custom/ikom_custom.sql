UPDATE res_currency SET position='before' WHERE name='MXN';
UPDATE res_lang SET grouping='[3,3,3,3,3,3,3,3,3,3,-2]', decimal_point = '.', thousands_sep=',' WHERE code='es_MX';
DELETE FROM res_groups WHERE name='Ikom_Nobody';
INSERT INTO res_groups (name) VALUES ('Ikom_Nobody');
INSERT INTO ir_ui_menu_group_rel (menu_id, gid) VALUES (111, (SELECT id FROM res_groups WHERE name='Ikom_Nobody'));