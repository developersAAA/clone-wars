# -*- coding: utf-8 -*-
{
    'name': 'Ikom Custom Purchase',
    'version': '1.0.0',
    'category': 'RH',
    'sequence': 1,
    'author': 'Gerardo A Lopez Vega @glopzvega',
    'website': 'http://www.desiteg.com',
    'summary': 'Modulo de personalizaciones del modulo de compras',
    'description': '''Modulo de personalizaciones del modulo de compras, este modulo traduce algunos menus que no estaban traducidos 
                    *Purchases -> Compras
                    *Suppliers -> Proveedores''',
    'depends': ["purchase"],    
    'data': [
             "purchase.sql"           
    ],    
    'installable': True,
    'active': True
}