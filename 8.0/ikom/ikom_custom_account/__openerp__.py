# -*- coding: utf-8 -*-
{
    'name': 'Ikom Custom Account',
    'version': '1.0.0',
    'category': 'RH',
    'sequence': 1,
    'author': 'Gerardo A Lopez Vega @glopzvega',
    'website': 'http://www.desiteg.com',
    'summary': 'Modulo de personalizaciones del modulo de contabilidad',
    'description': '''Modulo de personalizaciones del modulo de contabilidad,
                    el módulo modifica las siguientes vistas:
                    * res_partner_view - oculta algunos campos no requeridos como: 
                    ** posicion fiscal
                    Las funcionalidades que agrega este modulo es que agrega la opcion de 
                    permitir conciliacion para las cuentas que son de tipo Cliente o Proveedor.
                    Adicional a esto se traducen algunos menus que no se habian traducido.
                    ''',
    'depends': ["account"],
    'data': [
            "account.sql",
            "res_partner_view.xml",           
    ],    
    'installable': True,
    'active': False
}