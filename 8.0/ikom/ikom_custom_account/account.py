# -*- coding: utf-8 -*-
from openerp.osv import osv, fields

class account_account(osv.osv):
    _inherit = "account.account"
    
    def create(self, cr, uid, vals, context=None):
    
        if vals["type"] == "receivable" or vals["type"] == "payable":
            vals["reconcile"] = True
        
        res = super(account_account, self).create(cr, uid, vals, context=context)
        
        return res
        
    
account_account()