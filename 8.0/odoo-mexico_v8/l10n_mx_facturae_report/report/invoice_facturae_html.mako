<!DOCTYPE>
<html>
<head>
    <style type="text/css">
        ${css}
    </style>
</head>
<body>

    <% setLang(context.get('lang') or user.lang) %>
    %for o in objects :
        ${set_global_data(o)}
        <% dict_data = set_dict_data(o) %>
        <% dict_context_extra_data =  {}%>
        <table class="basic_table">
            <tr>
                <td style="vertical-align: top;">
                    ${helper.embed_image('jpeg',str(get_logo(o)),180, 85)}
                    
                    <div style="text-align: left;"><span style="font-weight: bold;font-size: 12pt">${ dict_data.get('Receptor', False) and dict_data.get('Receptor').get('@nombre', False) or ''|entity}<br></span>
                    <span style="font-size: 10pt" >${ dict_data.get('Receptor', False) and dict_data.get('Receptor').get('@rfc', False) or ''|entity}<br>
                    <%add_receptor = dict_data.get('Receptor', {}).get('Domicilio', {}) or {}%>
                    ${ add_receptor.get('@calle') or ''|entity},
                    No. Ext:${ add_receptor.get('@noExterior', False) or ''|entity},
                    No. Int:${ add_receptor.get('@noInterior', False) or ''|entity}<br>
                    ${ add_receptor.get('@colonia', False) or ''|entity},
                    C.P.:${ add_receptor.get('@codigoPostal', False) or ''|entity}<br>
                    ${ add_receptor.get('@localidad', False) or ''|entity}<br>
                    ${ add_receptor.get('@municipio', False) or ''|entity},
                    ${ add_receptor.get('@estado', False) or ''|entity}<br>
                    ${ add_receptor.get('@pais', False) or ''|entity}<br>
                    </div>
                </td>
                <td style="vertical-align: top;">
                    <table class="basic_table">
                        <tr>
                            <td width="10%">
                                
                            </td>
                            <td width="60%">
                                    <div style="text-align: center;"><span style="font-weight: bold;font-size: 12pt"> ${ dict_data.get('Emisor', False) and dict_data.get('Emisor').get('@nombre', False) or ''|entity}</span>
                                    <span style="font-size: 10pt" ><%dom_fis = dict_data.get('Emisor', {}).get('DomicilioFiscal', {}) or {}%>
                                    <br/>${ dom_fis.get('@calle', False) or ''|entity}
                                    ${ dom_fis.get('@noExterior', False) or ''|entity}
                                    ${ dom_fis.get('@noInterior', False) or ''|entity}
                                    ${ dom_fis.get('@colonia', False) or ''|entity}
                                    ${ dom_fis.get('@codigoPostal', False) or ''|entity}
                                    <br/>${ _("Localidad:")} ${ dom_fis.get('@localidad', False) or ''|entity}
                                    <br/>${ dom_fis.get('@municipio', False) or ''|entity}
                                    , ${ dom_fis.get('@estado', False) or ''|entity}
                                    , ${ dom_fis.get('@pais', False) or ''|entity}
                                    <br/>${_("RFC:")} ${dict_data.get('Emisor', False) and dict_data.get('Emisor').get('@rfc', False) or ''|entity}
                                    <br/>${ dict_data.get('Emisor', False) and dict_data.get('Emisor').get('RegimenFiscal', False) and dict_data.get('Emisor').get('RegimenFiscal').get('@Regimen', False) or ''|entity }
                                    <%emisor = dict_context_extra_data.get('emisor', {})%>
                                    %if emisor.get('phone', False) or emisor.get('fax', False) or emisor.get('mobile', False):
                                        <br/>${_("Tel&eacute;fono(s):")}
                                        ${emisor.get('phone', False) or ''|entity}
                                        ${emisor.get('fax', False)  and ',' or ''|entity} ${emisor.get('fax', False) or ''|entity}
                                        ${emisor.get('mobile', False) and ',' or ''|entity} ${emisor.get('mobile', False) or ''|entity}
                                    %endif
                                    </font>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="td_data_exp">

                            </td>
                            <td class="td_data_exp">
                                <div style="text-align: right;font-size: 10pt">
                                    <% from datetime import datetime %>
                                    <div style="text-align: center; background-color: #3333ff;"><span
                                          style="color: white;">&nbsp;&nbsp;&nbsp;&nbsp;
                                          FOLIO FISCAL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;
                                        </div>
                                    <%tfd = dict_data.get('Complemento', {}).get('TimbreFiscalDigital', {})%>
                                    ${ tfd.get('@UUID', 'No identificado')|entity }<br>
                                    Ref. Interna: ${ dict_data.get('@folio', 'No identificado')|entity }<br>
                                    Fecha de emision:${ dict_data.get('@fecha', 'No identificado')|entity }<br>
                                    Fecha de certificacion:${ tfd.get('@FechaTimbrado', False) and datetime.strptime(tfd.get('@FechaTimbrado').encode('ascii','replace'), '%Y-%m-%dT%H:%M:%S').strftime('%d/%m/%Y %H:%M:%S') or 'No identificado'|entity }<br>
                                    Serie del Certificado SAT:${ tfd.get('@noCertificadoSAT', 'No identificado') or 'No identificado'|entity }<br>
                                    Serie del Certificado del Contribuyente: ${ dict_data.get('@noCertificado', 'No identificado')|entity }<br>
                                <div/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table class="line" width="100%" border="1"></table>
        <table class="basic_table" style="font-size:11;">
            <tr>
                
                        </tr>
                    </table>
                    <table class="basic_table" style="border-bottom:1px solid #002966;">
                        <tr>
                            <%receptor = dict_context_extra_data.get('receptor', False)%>
                            %if receptor and (receptor.get('phone', False) or receptor.get('fax', False) or receptor.get('mobile', False)):
                                <td width="13%" class="cliente"><b>Telefono(s):</b></td>
                                <td width="55%" class="cliente">
                                    ${receptor.get('phone', False) or ''|entity}
                                    ${receptor.get('fax', False) and ',' or ''|entity}
                                    ${receptor.get('fax', False) or ''|entity}
                                    ${receptor.get('mobile', False) and ',' or ''|entity}
                                    ${receptor.get('mobile', False) or ''|entity}</font>
                            %endif
                            %if dict_context_extra_data.get('origin', False ):
                                <td width="9%" class="cliente"><b>Origen:</b></td>
                                <td width="23%" class="cliente"><b>${dict_context_extra_data.get('origin', False) or ''|entity}</b></td>
                            %endif
                        </tr>
                    </table>
                </td>
                <td width="19%" align="center">
                    <table class="basic_table" style="text-align:center;font-size: 8pt" border="0">
                        <% from datetime import datetime %>
                    </table>
                </td>
            </tr>
        </table>
        %if dict_data.get('Complemento',{}).get('Nomina', {}):
            <%nomina = dict_data.get('Complemento',{}).get('Nomina', {})%>
            <table width="100%">
                <table width="100%" class="basic_table" style="font-size:9; border:1.5px solid grey;">
                    <tr>
                        <td class="cliente"><b>${_('No. Identificaci&oacute;n')}</b></td><td class="cliente">${ nomina.get('@NumEmpleado', '') |entity }</td>
                        <td class="cliente"><b>${_('Puesto')}</b></td><td class="cliente">${ nomina.get('@Puesto', '') |entity }</td>
                        <td class="cliente"><b>${_('CURP')}</b></td><td class="cliente">${ nomina.get('@CURP', '') |entity }</td>
                    </tr>
                    <tr>
                        <td class="cliente"><b>${_('Riesgo de puesto')}</b></td><td class="cliente">${ nomina.get('@RiesgoPuesto', '') |entity }</td>
                        <td class="cliente"><b>${_('Departamento')}</b></td><td class="cliente">${ nomina.get('@Departamento', '') |entity }</td>
                        <td class="cliente"><b>${_('N&uacute;m. seguridad social')}</b></td><td class="cliente">${ nomina.get('@NumSeguridadSocial', '') |entity } </td>
                    </tr>
                </table>
            </table>
        %endif

        <table class="basic_table" style="color:#121212">
            <tr class="firstrow">
                <th width="10%">${_("Cant.")}</th>
                <th width="10%">${_("Unidad")}</th>
                <th>${_("Descripci&oacute;n")}</th>
                <th width="9%" >${_("P.Unitario")}</th>
                <th width="15%">${_("Importe")}</th>
            </tr>
            <%row_count = 1%>
            <% dict_lines =  dict_data.get('Conceptos', {}).get('Concepto', ())%>
            %if not isinstance(dict_lines, list):
                <% dict_lines =  [dict_lines]%>
            %endif
            %for dict in range(0,len(dict_lines)):
                %if (row_count%2==0):
                    <tr  class="nonrow">
                %else:
                    <tr>
                %endif
                    <td width="10%" class="number_td"><% qty = dict_lines[dict].get('@cantidad', '0.0') %> ${ formatLang(float(qty)) |entity}</td>
                    <td width="10%" class="basic_td"><% uni = dict_lines[dict].get('@unidad', '0.0') %>${ uni |entity}</td>
                    <td class="basic_td"><% desc = dict_lines[dict].get('@descripcion', '0.0') %>${ desc |entity}</td>
                    <td width="9%" class="number_td"><% vuni = dict_lines[dict].get('@valorUnitario', '0.0') %>${ dict_context_extra_data.get('symbol_currency', '') } ${ formatLang(float(vuni)) |entity}</td>
                    <td width="15%" class="number_td"><% imp = dict_lines[dict].get('@importe', '0.0') %>${ dict_context_extra_data.get('symbol_currency', '') } ${ formatLang(float(imp)) |entity}</td>
                    </tr>
                <%row_count+=1%>
            %endfor
        </table>
        <table align="right" width="30%" style="border-collapse:collapse">
            <tr>
                <td class="total_td">${_("Sub Total:")}</td>
                <td align="right" class="total_td">${ dict_context_extra_data.get('symbol_currency', '') } ${ formatLang(float(dict_data.get('@subTotal', '')))|entity}</td>
            </tr>
            <% desc_amount = float(dict_data.get('@descuento', 0.0)) %>
            %if desc_amount > 0:
            <tr>
                <td class="total_td">${_("Descuento:")}</td>
                <td align="right" class="total_td">${ dict_context_extra_data.get('symbol_currency', '') } ${ dict_data.get('@descuento', '') |entity}</td>
            </tr>
            %endif
            %if dict_data.get('Impuestos', {}).get('Traslados', False):
                <% dict_imp =  dict_data.get('Impuestos', {}).get('Traslados', {}).get('Traslado', ())%>
                %if not isinstance(dict_imp, list):
                    <% dict_imp =  [dict_imp]%>
                %endif
                %for imp in range(0,len(dict_imp)):
                    <% imp_amount = float(dict_imp[imp]['@importe']) %>
                    %if imp_amount > 0:
                        <tr>
                            <td class="tax_td">
                                <% imp_name = dict_imp[imp]['@impuesto'] %>
                                <% tasa = dict_imp[imp]['@tasa'] %>
                                <% text = imp_name+' ('+tasa+') %' %>${ text or '0.0'}
                            </td>
                            <td class="tax_td" align="right">
                                ${ dict_context_extra_data.get('symbol_currency', '') } ${formatLang(float(imp_amount)) or '0.0'|entity}
                            </td>
                        </tr>
                    %endif
                %endfor
            %endif
            %if dict_data.get('Impuestos', {}).get('Retenciones', False):
                <% dict_ret =  dict_data.get('Impuestos', {}).get('Retenciones', {}).get('Retencion', ())%>
                %if not isinstance(dict_ret, list):
                    <% dict_ret =  [dict_ret]%>
                %endif
                %for ret in range(0,len(dict_ret)):
                    <% ret_amount = float(dict_ret[ret]['@importe']) %>
                    %if ret_amount > 0:
                        <tr>
                            <td class="tax_td">
                                <% ret_name = dict_ret[ret]['@impuesto'] %>
                                ${_("Ret. ")} ${ ret_name or '' | entity }
                            </td>
                            <td class="tax_td" align="right">
                                ${ dict_context_extra_data.get('symbol_currency', '') } ${ ret_amount or '' | entity }
                            </td>
                        </tr>
                    %endif
                %endfor
            %endif
            <tr align="left">
                <td class="total_td"><b>${_("Total:")}</b></td>
                <td class="total_td" align="right"><b>${ dict_context_extra_data.get('symbol_currency', '') } ${ formatLang(float(dict_data.get('@total', '')))|entity}</b></td>
            </tr>
        </table>
        <table class="basic_table">
            <tr>
                <td class="tax_td">
                    ${_("IMPORTE CON LETRA:")}
                </td>
            </tr>
            <tr>
                <td class="center_td">
                    <% amount_in_text = amount_to_text(float(dict_data.get('@total', 0.0).encode('ascii','replace')),dict_data.get('@Moneda', '')) %>
                    <i>${ amount_in_text or ''|entity}</i>
                </td>
            </tr>
            <tr>
                <td class="center_td">
                    <div style="text-align: center; background-color: silver;"><span">&nbsp;&nbsp;&nbsp;&nbsp;
                                          Condiciones de la Venta&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>&nbsp;&nbsp;
                                        </div>
                </td>
            </tr>
        </table>
        %if dict_context_extra_data.get('payment_term', False) or dict_context_extra_data.get('comment', False):
            <table class="basic_table">
                %if dict_context_extra_data.get('payment_term', False):
                    <tr>
                        %if dict_context_extra_data.get('payment_term', False):
                            <td width="100%"><pre><font size="1"><b>Condición de pago:</b> ${dict_context_extra_data.get('payment_term', False) or '' |entity}
                            </font></pre></td>
                        %endif
                    </tr>
                %endif
                %if dict_context_extra_data.get('comment', False):
                    <tr>
                        %if dict_context_extra_data.get('comment', False):
                            <td width="100%"><pre><font size="1"><b>Comentarios adicionales:</b> ${dict_context_extra_data.get('comment', False) or '' |entity}</font></pre></td>
                        %endif
                    </tr>
                %endif
            </table>
        %endif
        
        </br>
        %if dict_context_extra_data.get('acc_banks', False):
            <table class="basic_table">
                <tr>
                    <td class="center_td">
                        ${_('Datos Bancarios')} ${dict_context_extra_data.get('name_emmiter', '')}
                    </td>
                </tr>
            </table>
            <table class="basic_table" rules="all">
                <tr>
                    <td class="data_bank_label" width="30%">${_('Banco / Moneda')}</td>
                    <td class="data_bank_label" width="20%">${_('N&uacute;mero de cuenta')}</td>
                    <td class="data_bank_label" width="30%">${_('Clave Interbancaria Estandarizada (CLABE)')}</td>
                    <td class="data_bank_label" width="20%">${_('Referencia')}</td>
                <tr>
                %for ab in dict_context_extra_data.get('acc_banks', []):
                    <tr>
                        <td class="center_td">${ab.get('name', '')|entity} ${ab.get('currency', False) and '/' or '' |entity} ${ab.get('currency', '') |entity}</td>
                        <td class="center_td">${ab.get('acc_number', '')|entity}</td>
                        <td class="center_td">${ab.get('clabe', '')|entity}</td>
                        <td class="center_td">${ab.get('reference', '')|entity}</td>
                    <tr>
                </tr>
                %endfor
            </table>
        %endif
        %if dict_data.get('Complemento', {}).get('TimbreFiscalDigital'):
            <table class="basic_table" >
                    <tr>
                        Condicion de Pago: ${ dict_data.get('@formaDePago', 'No identificado')|entity }&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Forma de Pago:${ dict_data.get('@formaDePago', 'No identificado')|entity }<br>
                        Metodo de Pago de Pago: ${ dict_data.get('@metodoDePago', 'No identificado')|entity }&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tipo de Comprobante: ${ dict_data.get('@tipoDeComprobante', 'No identificado')|entity }<br>
                        Lugar de Expedicion: ${ dict_data.get('@LugarExpedicion', '') |entity}<br>
                    </tr>

            </table>
        %endif
        %if dict_data.get('Complemento', {}).get('TimbreFiscalDigital'):
            <div style="page-break-inside:avoid; border:1.5px solid grey;">
                <table width="100%" class="datos_fiscales">
                    <tr>
                        <td align="right" rowspan="2">
                            <% img = create_qrcode(dict_data.get('Emisor', {}).get('@rfc', ''), dict_data.get('Receptor', {}).get('@rfc', ''), float(dict_data.get('@total', 0.0).encode('ascii','replace')), dict_data.get('Complemento', {}).get('TimbreFiscalDigital', {}).get('@UUID', '')) %>
                            ${helper.embed_image('jpeg',str(img),180, 180)}
                        </td>
                        <td valign="top" align="left">
                            <p class="cadena_with_cbb_cfd">
                            <b>${_('Sello Digital Emisor:')} </b><br/>
                            ${ dict_data.get('@sello', '')|entity}<br/>
                            <b>${_('Sello Digital SAT:')} </b><br/>
                            ${ dict_data.get('Complemento').get('TimbreFiscalDigital').get('@selloSAT', '')|entity}<br/>
                            <b>${_('Cadena original:')} </b><br/>
                            ${get_cadena_original(o)}</br>

                            </p>
                        </td>

                    </tr>
                    <tr>
                        <td style="vertical-align: center; text-align: center">
                            <p><b><font size="1">"Este documento es una representación impresa de un CFDI”</br>
                            CFDI, Comprobante Fiscal Digital por Internet</font></b></p>
                        </td>
                    </tr>
                </table>
            </div>
        %endif
        <p style="page-break-after:always"></p>
    %endfor
</body>
</html>
